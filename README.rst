PyAquila
========


PyAquila is a python package of models and analysis functions in python. Their
general purpose is to run model or run analysis and visualization for cosmology
and large scale structures in general.

Contributing to PyAquila
------------------------

Please copy the script "hooks/pre-commit" to ".git/hooks/pre-commit". This will
allow your computer to run pre-commit checks and ensure stylistic quality of
your code.

This hook requires the following command to be available:
- git
- clang-format (part of llvm/clang)
- yapf (either from your distribution or with ``pip3 install yapf``)