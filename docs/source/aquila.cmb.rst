aquila.cmb package
==================

Submodules
----------

aquila.cmb.analyse\_cluster module
----------------------------------

.. automodule:: aquila.cmb.analyse_cluster
   :members:
   :undoc-members:
   :show-inheritance:

aquila.cmb.make\_masks module
-----------------------------

.. automodule:: aquila.cmb.make_masks
   :members:
   :undoc-members:
   :show-inheritance:

aquila.cmb.projector module
---------------------------

.. automodule:: aquila.cmb.projector
   :members:
   :undoc-members:
   :show-inheritance:

aquila.cmb.tools module
-----------------------

.. automodule:: aquila.cmb.tools
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: aquila.cmb
   :members:
   :undoc-members:
   :show-inheritance:
