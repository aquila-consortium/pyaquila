from typing import Optional

class InvalidFile(ValueError):
    pass

def _make_pk(Lbox: Optional[float], power_args: dict, prefix: str, fname: str):
    from ..clustering import compute_power_spectrum
    import h5py as h5

    with h5.File(fname, mode="r") as ff:
        Ls = [Lbox]
        #Ls = list(ff[f"{prefix}scalars/L{i}"] for i in range(3))
        #if Ls[0] != Ls[1] or Ls[1] != Ls[2]:
        #   raise InvalidFile()
        _newbk, Pk = compute_power_spectrum(
            ff[f"{prefix}scalars/BORG_final_density"][...], Ls[0], **power_args
        )
    return _newbk, Pk


def make_powerspectrums(
    chain_dir: str,
    Lbox: Optional[float] = None,
    chain_kwargs={},
    progressbar=False,
    power_args={},
    pool=None,
    prefix: str = "",
):
    from ..utils import map_chain
    import h5py as h5
    from functools import partial

    mapper = map_chain(
        partial(_make_pk, Lbox, power_args, prefix),
        iter_args=(chain_dir,),
        iter_kwargs=chain_kwargs,
        progressbar=progressbar,
        pool=pool,
    )
    result = list(mapper)

    return result[0][0], list(map(lambda x: x[1], result))


def make_powerspectrum_plot(chain_dir: str, Lbox: float, chain_kwargs={}):
    import matplotlib.pyplot as plt

    Pk = make_powerspectrums(chain_dir, Lbox, chain_kwargs=chain_kwargs)
