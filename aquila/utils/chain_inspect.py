import errno


def detect_ncpus(path):
    """Detect the number of MPI tasks used to produce some files by BORG

    Args:
        path (str): Full base prefix path excluding the task part

    Raises:
        IOError

    Returns:
        int: number of MPI tasks
    """
    ncpu = 0
    try:
        while True:
            with open("%s_%d" % (path, ncpu), mode="rb") as f:
                ncpu += 1
    except IOError as e:
        if e.errno != errno.ENOENT:
            raise e

    return ncpu
