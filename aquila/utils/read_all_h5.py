# +
#   ARES/HADES/BORG Package -- ./scripts/ares_tools/read_all_h5.py
#   Copyright (C) 2014-2020 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2020 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2020)
#      Jens Jasche <j.jasche@tum.de> (2016-2017)
#      elsner <f.elsner@mpa-garching.mpg.de> (2017)
#
# +
import os
import numpy as np
import numexpr as ne
import h5py as h5

from .progress import progress, chain_iterator
from .state import default_slicer, read_group, read_attr_h5


def read_all_h5(fname, lazy=False):
    if lazy:
        f = h5.File(fname, mode="r")
        return read_group(f, lazy=True)
    else:
        with h5.File(fname, mode="r") as f:
            return read_group(f, lazy=False)


def grabber(obj, e):
    if len(e) == 1:
        return getattr(obj, e[0])
    else:
        return grabber(getattr(obj, e[0]), e[1:])


def read_chain_h5(
    path,
    element_list,
    start=0,
    step=1,
    err_up=0,
    slicer=default_slicer,
    prefix="mcmc",
    flexible=True,
):
    """
    read_chain_h5(path,element_list,start=0,step=1,err_up=0,slicer=default_slicer)

    Arguments:
      * path (str): path where you have the chain (mcmc_* files)
      * element_list (list of str): list of strings where the MCMC objects are stored. For
                      example, you have scalars.galaxy_nmean_0 for the nbar
                      parameter of the first catalog.
      * start (int): the first element of the chain to consider
      * step (int): if you want to thin the chain by "step"
      * err_up (int): whether to accept I/O errors when opening files up to the
        specified MCMC id
      * slicer (function): a lambda function that can only take a subset of the array of
        the specified MCMC object. For example, it can be lambda d: ``d[:,:,64]``,
        to indicate only the plane 64 in the 3d grid that is being loaded.

    Returns:
      * a columned numpy array ``a``. You have one column for each element_id of
        the element_list.  You can access one of the column like this:
        ``a["scalars.galaxy_nmean_0"]``
    """
    i = start
    b = [[] for e in element_list]
    egrab = [e.split(".") for e in element_list]
    for fname in progress(
        chain_iterator(path, start=start, step=step, err_up=err_up, prefix=prefix)
    ):
        try:
            a = read_attr_h5(fname, egrab, slicer=slicer)
        except OSError:
            if not flexible:
                raise
            else:
                break
        for j, e in enumerate(egrab):
            b[j].append(grabber(a, e))

    dtype = [(e, t[0].dtype, t[0].shape) for e, t in zip(element_list, b)]

    arr = np.empty(len(b[0]), dtype=dtype)
    for e, q in zip(element_list, b):
        arr[e] = q
    return arr


def build_power_histogram(
    path, start=0, step=1, Nhisto=100, quiet=True, logP=True, Prange=(0.1, 1e5)
):
    """
    Use the scalars.powerspectrum mcmc element to build the PDF of the posterior
    powerspectrum

    Arguments:
       * path
       * start
       * step
       * Nhisto: number of bins for each k mode of the P(k) histogram
       * quiet:
       * logP: whether you want to use a log scale for plotting
       * Prange: a tuple for giving the entire P range to represent

     Returns:
       * a tuple: t=(kmodes, Pgrid, Pk_pdf_values) which is directly usable in pcolormesh
         like ``pcolormesh(*t)``
    """
    # print path+ "/restart.h5_0"
    for _, scalars in explore_chain(path, start, end=start + 1, step=1, quiet=quiet):
        Nk = scalars["powerspectrum"].size
    with h5.File(path + "/restart.h5_0", mode="r") as f:
        k_mode = f["/scalars/k_modes"][:]

    Phisto = np.zeros((Nk, Nhisto), dtype=np.int)
    if logP:
        logPmin = np.log10(Prange[0])
        DeltaLog = np.log10(Prange[1] / Prange[0]) / (Nhisto)

        def transform(P):
            return (np.log10(P) - logPmin) / DeltaLog

    else:
        Pmin = Prange[0]
        DeltaP = (Prange[1] - Prange[0]) / (Nhisto)

        def transform(P):
            return (P - Pmin) / DeltaP

    for n, scalars in explore_chain(path, start, step=step, quiet=quiet):
        P = scalars["powerspectrum"][:]
        iP = np.floor(transform(P))
        ik = np.where((np.isnan(iP) == False) * (iP >= 0) * (iP < Nhisto))
        iP = iP[ik].astype(np.int)
        for i, j in zip(ik[0], iP):
            Phisto[i, j] = Phisto[i, j] + 1

    k_mode = k_mode[:, None].repeat(Nhisto, axis=1)
    if logP:
        Pg = 10 ** ((np.arange(Nhisto) + 0.5) * DeltaLog + logPmin)
    else:
        Pg = (np.arange(Nhisto) + 0.5) * DeltaP + Pmin

    Pg = Pg[None, :].repeat(Nk, axis=0)
    return k_mode, Pg, Phisto


def read_chain_complex_avg_dev(
    path,
    op,
    start=0,
    end=-1,
    do_dev=False,
    step=1,
    slicer=default_slicer,
    prefix="mcmc",
):
    """
    Compute mean and standard deviation of the given element_list

    Arguments:
      * path (str):
      * op (function):
      * element_list (list):
      * start (int):
      * do_dev (bool): boolean for computing the standard deviation (or not)
      * slicer (function):

    Returns:
      * a columned numpy array. Each column has a name that corresponds to an
        element with an additional dimension. For example,
        ``a['scalars.galaxy_nmean_0'][0] -> mean``,
        ``a['scalars.galaxy_nmean_0'][1]`` is the standard deviation.
    """
    i = 0
    b = None
    bd = None
    try:
        for fname in progress(
            chain_iterator(path, start=start, step=step, end=end, prefix=prefix)
        ):
            with h5.File(fname, mode="r") as ff:
                if b is None:
                    b = op(ff)
                    if do_dev:
                        bd = np.zeros(b.shape)
                else:
                    data = op(ff)
                    ne.evaluate(
                        "r*a+k*c",
                        dict(
                            k=1 / float(i + 1), r=(float(i) / float(i + 1)), a=b, c=data
                        ),
                        out=b,
                    )
                if do_dev and i > 1:
                    ne.evaluate(
                        "k*(xn-mun)**2 + f*bdn",
                        dict(
                            k=1 / float(i),
                            f=float(i) / float(i + 1),
                            xn=data,
                            mun=b,
                            bdn=bd,
                        ),
                        out=bd,
                    )
                i += 1

    except OSError:
        pass

    bd = np.sqrt(bd)
    return b, bd


def read_chain_avg_dev(
    path,
    element_list,
    start=0,
    end=-1,
    do_dev=False,
    operator=lambda x: x,
    step=1,
    slicer=default_slicer,
    prefix="mcmc",
    err_up=0,
):
    """
    Compute mean and standard deviation of the given ``element_list``

    Arguments:
      * path (str):
      * element_list (list):
      * start (int):
      * do_dev (bool): boolean for computing the standard deviation (or not)
      * operator (function): applies the operator on all the elements before
        computing the mean and standard deviation.
      * slicer (function):

    Returns:
      * a columned numpy array. Each column has a name that corresponds to an
        element with an additional dimension. For example,
        ``a['scalars.galaxy_nmean_0'][0] -> mean``,
        ``a['scalars.galaxy_nmean_0'][1]`` is the standard deviation.
    """
    i = 0
    b = [None for e in element_list]
    bd = [None for e in element_list]
    egrab = [e.split(".") for e in element_list]
    for fname in progress(
        chain_iterator(
            path, start=start, step=step, end=end, err_up=err_up, prefix=prefix
        )
    ):
        a = read_attr_h5(fname, egrab, slicer=slicer)
        for j, e in enumerate(egrab):
            if b[j] is None:
                b[j] = operator(grabber(a, e))
                if do_dev:
                    bd[j] = np.zeros(b[j].shape)
            else:
                data = operator(grabber(a, e))
                ne.evaluate(
                    "r*a+k*c",
                    dict(
                        k=1 / float(i + 1), r=(float(i) / float(i + 1)), a=b[j], c=data
                    ),
                    out=b[j],
                )
                if do_dev and i > 0:
                    ne.evaluate(
                        "k*(xn-mun)**2 + f*bdn",
                        dict(
                            k=1 / float(i),
                            f=float(i) / float(i + 1),
                            xn=data,
                            mun=b[j],
                            bdn=bd[j],
                        ),
                        out=bd[j],
                    )
            i += 1

    dtype = [(e, t.dtype, t.shape) for e, t in zip(element_list, b)]

    arr = np.empty(2 if do_dev else 1, dtype=dtype)
    for e, q, q2 in zip(element_list, b, bd):
        arr[e][0] = q
        if do_dev:
            arr[e][1] = np.sqrt(q2)
    return arr
