import h5py as h5


def default_slicer(x):
    return x[...]


class Bunch(object):
    """Helper class to store hdf5 objects. Closing lazily the file when last ref is dropped."""

    def __init__(self, **kwds):
        self._lazy = False
        self._group = None
        self.__dict__.update(**kwds)

    def __del__(self):
        if self._lazy:
            # close the file/group
            if isinstance(self._group, h5.File):
                print("Closing HDF5 file")
                try:
                    self._group.close()
                except:
                    # Eat all exceptions
                    pass

    def insert(self, aname, data):
        if len(aname) == 1:
            self.__dict__.update({aname[0]: data})
        else:
            if aname[0] in self.__dict__:
                k = self.__dict__[aname[0]]
            else:
                k = Bunch()
                self.__dict__.update({aname[0]: k})
            k.insert(aname[1:], data)


def read_group(g, lazy=False):
    m = {}
    for k in g:
        if hasattr(g[k], "keys"):
            m[k] = read_group(g[k], lazy=lazy)
        else:
            if lazy:
                m[k] = g[k]
            else:
                m[k] = g[k][:]
    if lazy:
        m["_group"] = g
    m["_lazy"] = lazy

    return Bunch(**m)


def read_attr_group(g, ename, bunch, slicer=default_slicer):
    for a in ename:
        g = g[a]
    if bunch == None:
        bunch = Bunch()

    bunch.insert(ename, slicer(g))
    return bunch


def read_attr_h5(fname, egroup, slicer=default_slicer):
    with h5.File(fname, mode="r") as f:
        b = None
        for e in egroup:
            b = read_attr_group(f, e, b, slicer=slicer)
    return b


__all__ = ["read_attr_h5", "read_attr_group", "read_group", "default_slicer"]
