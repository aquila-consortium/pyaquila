import h5py as h5
import numpy as np
import os

#
# FIXME: Not sure what that function does...
#


def chain_compute_xcor(path, start=0, Nbins=100):
    import cosmotool as ct
    import numexpr as ne

    i = 0

    with h5.File(os.path.join(path, "restart.h5_0"), mode="r") as f:
        L0 = f["scalars"]["L0"][:]
        L1 = f["scalars"]["L1"][:]
        L2 = f["scalars"]["L2"][:]
        N0 = int(f["scalars"]["N0"][:])
        N1 = int(f["scalars"]["N1"][:])
        N2 = int(f["scalars"]["N2"][:])

    ix = np.fft.fftfreq(N0, d=1.0 / L0)[:, None, None]
    iy = np.fft.fftfreq(N1, d=1.0 / L1)[None, :, None]
    iz = np.fft.fftfreq(N2, d=1.0 / L2)[None, None, :]
    r2 = ne.evaluate("sqrt(ix**2+iy**2+iz**2)")
    rmax = r2.max()
    ir = (r2 * Nbins / rmax).astype(np.int32).ravel()
    xi = []
    W = np.bincount(ir, minlength=Nbins)

    fft = ct.CubeFT(L0, N0)

    while True:
        try:
            if i % 10 == 0:
                print(i)
            fname = os.path.join(path, "mcmc_%d.h5" % (i + start))
            with h5.File(fname, mode="r") as f:
                fft.density = f["scalars"]["s_field"][:]

            fft.rfft()
            ne.evaluate(
                "complex(real(d)**2 + imag(d)**2, 0)",
                local_dict={"d": fft.dhat},
                out=fft.dhat,
                casting="unsafe",
            )
            fft.irfft()

            xi.append(np.bincount(ir, weights=fft.density.ravel(), minlength=Nbins))
            i += 1

        except Exception as e:
            print(repr(e))
            break

    xi = np.array(xi) / W

    r = np.arange(Nbins) * rmax / Nbins
    return r, xi
