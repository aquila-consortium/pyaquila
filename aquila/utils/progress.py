import h5py as h5
from .notebook import is_notebook

try:
    import tqdm

    if is_notebook():
        u_tqdm = tqdm.tqdm_notebook
    else:
        u_tqdm = tqdm.tqdm

    def progress(iterator, total=None):
        if total is None:
            L = list(iterator)
            return u_tqdm(L)
        else:
            return u_tqdm(iterator, total=total)

except:

    def progress(iterator):
        for i, q in enumerate(iterator):
            if (i % 100) == 0:
                print(i)
            yield q


def chain_iterator(
    path, start=0, step=1, err_up=0, end=-1, prefix="mcmc", need_id=False
):
    """Iterate through each element of an MCMC chain from ARES/BORG

    Args:
        path (str): Base prefix path
        start (int, optional): start identifier
        step (int, optional): thinning of the chain. Defaults to 1.
        err_up (int, optional): Accept error reading file up to this id. Defaults to 0.
        end (int, optional): Last element of the chain to consider. Defaults to -1 (all).
        prefix (str, optional): Prefix of the file. Defaults to "mcmc".
        need_id (bool, optional): Indicate whether the generator must also yield the id. Defaults to False.

    Yields:
        filename: path of the chain element if need_id==False
        i,filename: chain id and path of the chain element if need_id==True

    """
    import os

    i = start
    while True:
        fname = os.path.join(
            path,
            "%s_%d.h5"
            % (
                prefix,
                i,
            ),
        )
        try:
            os.stat(fname)
        except IOError:
            if i >= err_up:
                return
            else:
                i += step
                continue

        if need_id:
            yield (i, fname)
        else:
            yield fname
        i += step
        if end > 0 and i > end:
            break


def explore_chain(path, start, end=-1, step=1, quiet=True):
    """
    Arguments:
      * path
      * start
      * end
      * step

    Returns:
      * iterator with hdf5 object. Example:
           for id, h5_group in explore_chain(".", 0):
              mean = h5_group['galaxy_nmean_0'][0]
              # Then do stuff with "mean"
    """
    n = int(start)
    nmax = int(end)
    step = int(step)
    k = 0

    while (nmax == -1) or (n < nmax):
        p = path + "/mcmc_%d.h5" % n
        if not quiet and (k % 100) == 0:
            print("%d" % n)
        try:
            f = h5.File(p, mode="r")
        except Exception as e:
            print(e)
            break

        try:
            yield n, f["scalars"]
        finally:
            f.close()

        n += step
        k += 1


def map_chain(
    func: object, iter_args=(), iter_kwargs={}, progressbar: bool = False, pool=None
):
    """Map the func (callable with a single string argument) over the entire chain.

    The arguments to chain_iterator may be passed through iter_args and iter_kwargs.

    Arguments:
        * func: callable object/function
        * iter_args (tuple): arguments for chain_iterator
        * iter_kwargs (dict): keyword arguments for chain_iterator
        * progressbar (bool): whether to decorate with a progress bar

    Returns:
        iterator over the chain. It may be passed to a list constructor.

    """

    iter = chain_iterator(*iter_args, **iter_kwargs)

    if pool is None:
        if progressbar:
            iter = progress(iter)
        for fname in iter:
            yield func(fname)
    else:
        iter = list(iter)
        for result in progress(pool.imap(func, iter), total=len(iter)):
            yield result


__all__ = ["progress", "chain_iterator", "explore_chain", "u_tqdm", "map_chain"]
