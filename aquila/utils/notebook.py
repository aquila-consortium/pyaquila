"""
This module includes specific ipython notebook management.
"""
from IPython import get_ipython


def is_notebook():
    """Check if the user is running in a Web browser environment

    Returns:
        bool: True if jupyter notebook/qtconsole
    """
    try:
        shell = get_ipython().__class__.__name__
        if shell == "ZMQInteractiveShell":
            return True  # Jupyter notebook or qtconsole
        elif shell == "TerminalInteractiveShell":
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False  # Probably standard Python interpreter
