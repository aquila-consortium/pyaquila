import numpy as np

from .state import read_attr_h5
from .read_all_h5 import grabber


def rebuild_spliced_h5(path, element_list, ncpu, verbose=False, flex_cpu=False):
    """Rebuild a set of fields which are spliced across different hdf5 files.
    The use of this function is typically dedicated to the analysis of MPI run.

    Parameters
    ----------
    path         : string
                   base path for the set of hdf5 file. A suffix "_[CPUID]" will be appended.
    element_list : list of string
                   list of elements to rebuild from the set of files
    ncpu         : int
                   number of cpus for the run
    verbose      : boolean
                   if True the code will run with verbose output
    flex_cpu     : boolean
                   if True, ncpu is understood as the maximum number of cpus. If the code does
                   find a file then it stops the rebuilding without failing.

    Returns
    -------
       dictionnary
       each element name is a key, and the value is a numpy array. The arrays are concatenated
       according to their first axis.
    """
    b = [None for e in element_list]
    egrab = [e.split(".") for e in element_list]

    for cpu in range(ncpu):
        fname = path + "_%d" % cpu
        if verbose:
            print("Loading CPU file '%s'" % fname)
        try:
            a = read_attr_h5(fname, egrab)
        except OSError:
            if flex_cpu:
                break
            raise
        for j, e in enumerate(egrab):
            if b[j] is None:
                b[j] = grabber(a, e)
            else:
                b[j] = np.append(b[j], grabber(a, e), axis=0)

    # dtype = [(e, t[0].dtype, t[0].shape) for e, t in zip(element_list, b)]
    arr = {}
    for e, q in zip(element_list, b):
        arr[e] = q
    return arr
