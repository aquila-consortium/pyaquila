"""
aquila.utils is subpackage of various utilities, notably to inspect BORG MCMC chains

"""

from .notebook import is_notebook
from .progress import progress, explore_chain, chain_iterator, map_chain
from .read_all_h5 import (
    read_all_h5,
    read_chain_avg_dev,
    read_chain_complex_avg_dev,
    read_chain_h5,
)
from .rebuild_split_arrays import rebuild_spliced_h5
from .chain_inspect import detect_ncpus
