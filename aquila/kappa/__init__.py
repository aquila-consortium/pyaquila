def read_ini_file(fname):
    import configparser

    config = configparser.ConfigParser()

    config.read(fname)
    return config["system"], config["cosmology"]


def run():
    from tqdm import tqdm
    from scipy.interpolate import interp1d
    import astropy.cosmology as aco
    import h5py as h5
    import cosmotool as ct
    import astropy.coordinates as acoord
    import argparse
    import sys
    import numpy as np

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--samples",
        help="Which samples to consider 'start,end,step'",
        required=True,
        type=str,
        default=None,
    )
    parser.add_argument("--mcmc", help="Path of the MCMC chain", default=".")
    parser.add_argument("--output", help="Output directory", default=".")
    parser.add_argument("--Nside", help="Nside output", type=int, default=128)
    parser.add_argument("--inifile", help="BORG ini file", type=str, required=True)
    parser.add_argument(
        "--min-distance", help="Minimum distance", type=float, required=True
    )
    parser.add_argument(
        "--max-distance", help="Maximum distance", type=float, required=True
    )
    parser.add_argument("--file-format", type=str, default="{}_{:04d}.h5")
    parser.add_argument("--prefix", type=str, default="output")
    parser.add_argument("--density-field", type=str, default="final_density")
    parser.add_argument("--density-mesh-size", type=str)
    parser.add_argument("--density-normalize", default=False, action="store_true")
    args = parser.parse_args()

    samples = list(int(s) for s in args.samples.split(","))
    if len(samples) != 3:
        print(f"'samples' must be a 3-tuple of integer. It was \"{args.samples}\"")
        sys.exit(1)
    samples_start, samples_end, samples_step = samples

    config_base, config_cosmo = read_ini_file(args.inifile)
    if args.density_mesh_size is not None:
        Nmesh = list(int(s) for s in args.density_mesh_size.split("x"))
        if len(Nmesh) != 3:
            print("'density-mesh-size' must be a 3-tuple of integer.")
            sys.exit(1)
        N0, N1, N2 = Nmesh
    else:
        N0, N1, N2 = tuple(int(config_base[f"N{n}"]) for n in range(3))

    if N0 != N1 or N0 != N2:
        print(f"Only cubic mesh is supported: N={N0},{N1},{N2}")
        sys.exit(1)

    L0, L1, L2 = tuple(float(config_base[f"L{n}"]) for n in range(3))
    if L0 != L1 or L0 != L2:
        print(f"Only cubic mesh is supported: L={L0},{L1},{L2}")
        sys.exit(1)

    corners = tuple(float(config_base[f"corner{n}"]) for n in range(3))

    N = N0
    L = L0
    h0 = float(config_cosmo["h100"])
    H0 = h0 * 100.0
    Om0 = float(config_cosmo["omega_m"])
    min_distance = args.min_distance * N / L
    max_distance = args.max_distance * N / L

    shifter = -np.array(corners) * N / L - 0.5 * N

    ix = np.arange(N) * L / N + corners[0]
    iy = np.arange(N) * L / N + corners[1]
    iz = np.arange(N) * L / N + corners[2]

    ix, iy, iz = np.meshgrid(ix, iy, iz, indexing="ij")
    d_c = np.sqrt(ix**2 + iy**2 + iz**2)

    cosmo = aco.FlatLambdaCDM(H0, Om0)

    zlist = np.linspace(0, 5, 10000)
    dlist = cosmo.comoving_distance(zlist)
    dcmb = cosmo.comoving_distance(1100).value
    a_func = interp1d(dlist, 1 / (1 + zlist))
    z_func = interp1d(dlist, zlist)

    weight_field = (
        ((dcmb - d_c / h0) / dcmb)
        * d_c
        / h0
        / a_func(d_c / h0)
        * 1.5
        * Om0
        * (H0 / 3e5) ** 2
        * (L / h0 / N)
    )

    for snapid in tqdm(range(samples_start, samples_end, samples_step)):
        with h5.File(args.file_format.format(args.prefix, snapid), mode="r") as ff:
            density = ff[args.density_field][:]

        if args.density_normalize:
            density = density / np.average(density) - 1

        density *= weight_field

        np.save(
            f"kappa_{snapid}.npy",
            ct.spherical_projection(
                args.Nside,
                density,
                min_distance,
                max_distance,
                shifter=shifter,
                integrator_id=1,
                booster=100,
            ),
        )
