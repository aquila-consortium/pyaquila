# +
# +
import struct
import h5py as h5
import numpy as np
import os
import time
import socket
import subprocess
import sys
import matplotlib.pylab as plt
from shutil import copyfile

from ..utils import rebuild_spliced_h5


class ConstrainedSimulation:
    """This class allows to read BORG output to generate plug-and-play configuration and initial conditions for heavy N-body simulation code.

    It relies on MUSIC to do part of the heavy lifting.
    """

    def __init__(
        self,
        path="",
        restart_file="restart.h5",
        mcmc_file_base="mcmc_",
        MUSIC_EXECUTABLE="",
        out_dir="",
        which_simulator="Gadget",
        fac_res=2,
        version=2,
        select_inner_region=False,
        anti_sim=0,
    ):

        print("************************************")
        print("**    Generate constrained Sims   **")
        print("**              with              **")
        print("**              BORG              **")
        print("************************************")

        self.path = path
        self.mcmc_file_base = mcmc_file_base
        self.restart_file = restart_file
        self.MUSIC_EXECUTABLE = MUSIC_EXECUTABLE
        self.fac_res = fac_res
        self.select_inner_region = select_inner_region
        self.anti_sim = anti_sim

        self.simulator = which_simulator
        if not version in [1, 2]:
            raise ValueError("invalid version of the IC")
        self.version = version

        self.out_dir = out_dir

        self.machine_name = socket.gethostname()
        self.date = time.strftime("%d%m%Y_%h")

        # set whitenoise file
        self.fname_white_noise = "wn.dat"
        # set output directory
        if self.out_dir == "":
            self.out_dir = os.path.join(
                ".", "borg_resim_at_" + self.machine_name + "_" + self.date
            )
        self.out_dir = os.path.abspath(self.out_dir)

        # create directory structure
        self.create_directory_structure()

        fbase = os.path.join(self.path, self.restart_file)
        self.ncpu = self.find_ncpu(fbase)

        # set cosmo
        self.cosmo = {
            "Tcmb": 2.728,
            "Omega_m": 0.307,
            "Omega_L": 0.693,
            "Omega_b": 0.04825,
            "H0": 70.5,
            "sigma_8": 0.8288,
            "nspec": 0.9611,
            "A_s": 0,
        }

        with h5.File(os.path.join(self.path, self.restart_file + "_0"), mode="r") as f:
            self.L0 = f["/scalars/L0"][:][0]
            self.L1 = f["/scalars/L1"][:][0]
            self.L2 = f["/scalars/L2"][:][0]
            self.N0 = f["/scalars/N0"][:][0].astype(int)
            self.N1 = f["/scalars/N1"][:][0].astype(int)
            self.N2 = f["/scalars/N2"][:][0].astype(int)
            # update cosmological parameters from run
            caux = f["scalars/cosmology"][0]
            self.retrieve_cosmology(caux)

        self.volume = self.L0 * self.L1 * self.L2
        if version == 1:
            self.key_modes = rebuild_spliced_h5(
                os.path.join(self.path, self.restart_file),
                ["scalars.k_keys"],
                self.ncpu,
            )["scalars.k_keys"]
        else:
            self.key_modes = None

        # set start redshift of simulation
        self.zstart = 69.0

    def create_directory_structure(self):
        if not os.path.exists(self.out_dir):
            print("creating directory: ", self.out_dir)
            os.makedirs(self.out_dir + "/resim/")
            print("creating directory: ", self.out_dir + "/resim/")
            os.makedirs(self.out_dir + "/ic/")
            print("creating directory: ", self.out_dir + "/ic/")
            os.makedirs(self.out_dir + "/outputlists/")
            print("creating directory: ", self.out_dir + "/outputlists/")
            os.makedirs(self.out_dir + "/white_noise/")
            print("creating directory: ", self.out_dir + "/white_noise/")
        else:
            if not os.path.exists(self.out_dir + "/resim/"):
                os.makedirs(self.out_dir + "/resim/")
                print("creating directory: ", self.out_dir + "/resim/")
            if not os.path.exists(self.out_dir + "/ic/"):
                os.makedirs(self.out_dir + "/ic/")
                print("creating directory: ", self.out_dir + "/ic/")
            if not os.path.exists(self.out_dir + "/outputlists/"):
                os.makedirs(self.out_dir + "/outputlists/")
                print("creating directory: ", self.out_dir + "/outputlists/")
            if not os.path.exists(self.out_dir + "/white_noise/"):
                os.makedirs(self.out_dir + "/white_noise/")
                print("creating directory: ", self.out_dir + "/white_noise/")

    def retrieve_cosmology(self, caux):
        self.cosmo["Omega_m"] = caux["omega_m"]
        self.cosmo["Omega_b"] = caux["omega_b"]
        self.cosmo["Omega_L"] = caux["omega_q"]
        self.cosmo["H0"] = caux["h"] * 100
        self.cosmo["sigma_8"] = caux["sigma8"]
        self.cosmo["nspec"] = caux["n_s"]
        try:
            self.cosmo["A_s"] = caux["A_s"]
        except:
            self.cosmo["A_s"] = 0

        sig8 = self.cosmo["sigma_8"]
        print(f"Sigma8 is {sig8}")
        if self.cosmo["sigma_8"] == 0.0:
            print("Sigma8 is not computed in the chain. Deriving it from A_s")
            try:
                import aquila_borg as borg

                cpar = borg.cosmo.CosmologicalParameters()
                cpar.omega_m = caux["omega_m"]
                cpar.omega_b = caux["omega_b"]
                cpar.omega_q = caux["omega_q"]
                cpar.h = caux["h"]
                cpar.A_s = caux["A_s"]
                cpar.sigma8 = 0
                cpar.n_s = caux["n_s"]
                cpar.A_s = caux["A_s"]
                cc = borg.cosmo.ClassCosmo(cpar, 100, 1.0)
                cc.computeSigma8()
                self.cosmo["sigma_8"] = cc.getCosmology()["sigma_8"]
                print(f"Found sigma8={self.cosmo['sigma_8']}")
            except ImportError:
                print("Could not find BORG to compute sigma8")
                raise

    def gen_music_param_file(self):
        import math

        ngrid = int(math.log(self.N0, 2))
        nincr = int(math.log(self.fac_res, 2))

        txt = "[setup]\n"
        if (self.L0 == self.L1) * (self.L0 == self.L2):
            txt += "boxlength=" + str(self.L0) + "\n"
        txt += "zstart=" + str(self.zstart) + "\n"
        txt += "region=box\n"
        txt += "levelmin=" + str(ngrid + nincr) + "\n"
        txt += "levelmax=" + str(ngrid + nincr) + "\n"
        txt += "levelmin_TF=" + str(ngrid + nincr) + "\n"
        txt += "force_equal_extent=no\n"
        txt += "padding=2\n"
        txt += "overlap=2\n"
        txt += "align_top=no\n"
        txt += "periodic_TF=yes\n"
        txt += "baryons=no\n"
        txt += "use_2LPT=yes\n"
        txt += "use_LLA=no\n"

        txt += "[box]\n"

        txt += "[cosmology]\n"
        txt += "Tcmb=" + str(self.cosmo["Tcmb"]) + "\n"
        txt += "Omega_m=" + str(self.cosmo["Omega_m"]) + "\n"
        txt += "Omega_L=" + str(self.cosmo["Omega_L"]) + "\n"
        txt += "Omega_b=" + str(self.cosmo["Omega_b"]) + "\n"
        txt += "H0=" + str(self.cosmo["H0"]) + "\n"
        txt += "sigma_8=" + str(self.cosmo["sigma_8"]) + "\n"
        txt += "nspec=" + str(self.cosmo["nspec"]) + "\n"
        txt += "transfer=eisenstein\n"
        txt += "YHe=0.248\n"

        txt += "[random]\n"
        if (self.N0 != self.N1) * (self.N0 != self.N2):
            print("Error: need equidistant box")
            exit()

        # txt+='seed['+ str(ngrid) +']= 50370 \n'
        txt += (
            "seed["
            + str(ngrid)
            + "]="
            + self.out_dir
            + "/white_noise/"
            + self.fname_white_noise
            + " \n"
        )

        grafic_sign = self.version == 1

        if self.anti_sim == 1:
            print("****************************")
            print("ANTI SIMULATION INITIALIZED")
            print("****************************")
            grafic_sign = not grafic_sign

        txt += f"grafic_sign={'true' if grafic_sign else 'false'}\n"
        txt += "restart=false\n"

        txt += "[output]\n"
        # choose Gadget2 format consistently
        if self.simulator == "RAMSES":
            txt += "format=grafic2\n"
            txt += "filename=" + self.out_dir + "/ic/ramses_ic" + "\n"
            txt += "ramses_nml=yes\n"
        elif self.simulator == "Gadget" or self.simulator == "Gadget4":
            txt += "format=gadget2\n"
            txt += "filename=" + self.out_dir + "/ic/ic.gad" + "\n"
            txt += "gadget_num_files=1\n"
            txt += "gadget_cell_centered=true\n"
        elif self.simulator == "WHITE":
            pass
        else:
            print("Error simulator:", self.simulator, "not known")
            exit()

        txt += "[poisson]\n"
        txt += "fft_fine=yes\n"
        txt += "laplace_order=6\n"
        txt += "grad_order=6\n"
        txt += "accuracy=1e-5\n"
        txt += "pre_smooth=3\n"
        txt += "post_smooth=3\n"
        txt += "smoother=gs\n"

        return txt

    def gen_param_gadget4_file(self):
        print("Generate Gadget-4 parameter file...")
        if (self.L0 != self.L1) or (self.L0 != self.L2):
            print("Error: cannot setup parameter file for non-cubic Domain...")
            print("Stopping: Preparing Gadget parameter file!")
            exit()

        txt = f"""
%  Relevant files
InitCondFile  {self.out_dir}/ic/ic.gad
OutputDir     {self.out_dir}/resim/outputs
EnergyFile   energy.txt
InfoFile    info.txt
TimingsFile timings.txt
CpuFile     cpu.txt
TimebinFile timebin.txt
SnapshotFileBase  snap_borg
RestartFile     restart
% CPU-time limit
MaxMemSize    4096
TimeLimitCPU    36000

ResubmitOn        0
ResubmitCommand   auto_submit.sh"

% Code options

ICFormat             2
SnapFormat           3
ComovingIntegrationOn 1

NumFilesPerSnapshot 1
NumFilesWrittenInParallel 2
MaxFilesWithConcurrentIO  1
CoolingOn       0
StarformationOn 0

%  Caracteristics of run

TimeBegin           {1.0 / (1.0 + self.zstart)}
TimeMax             1.0     % end at z=0
Omega0       {self.cosmo["Omega_m"]} % total matter density
OmegaLambda  {self.cosmo["Omega_L"]}
OmegaBaryon  {self.cosmo["Omega_b"]}
Hubble   100.
HubbleParam   {self.cosmo["H0"]/100}
BoxSize      {self.L0}
PeriodicBoundariesOn 1

% Softening lengths
MinGasHsmlFractional 0.1  % minimum gas smoothing in terms of the gravitational softening length\n"
SofteningGas       260.0
SofteningHalo      260.0
SofteningDisk      0.0
SofteningBulge     0.0
SofteningStars     260.0
SofteningBndry   260.0         % this will be the black hole particles2

SofteningGasMaxPhys    260.0
SofteningHaloMaxPhys   260.0
SofteningDiskMaxPhys   0.0
SofteningBulgeMaxPhys  0.0
SofteningStarsMaxPhys  260.0
SofteningBndryMaxPhys  260.0

% Output frequency

OutputListOn        0
OutputListFilename  {self.out_dir}/outputlists/outputs.txt

TimeBetSnapshot       1.1
TimeOfFirstSnapshot   0.1

CpuTimeBetRestartFile 7200.0  % every 2 hours

TimeBetStatistics      0.5

MaxRMSDisplacementFac 0.25

% Accuracy of time integration

TypeOfTimestepCriterion 0

ErrTolIntAccuracy      0.05

MaxSizeTimestep     0.05
MinSizeTimestep     1.0e-7

% Tree algorithm and force accuracy

ErrTolTheta                           0.5
ErrTolThetaMax                        1.0
ErrTolForceAcc                        0.0025
TopNodeFactor                         2.5

ActivePartFracForNewDomainDecomp      0.01


TypeOfOpeningCriterion  1
ErrTolForceAcc          0.005

TreeDomainUpdateFrequency 0.1

%  Parameters of SPH

DesNumNgb           33
MaxNumNgbDeviation  2

ArtBulkViscConst    1.0

InitGasTemp       145.0  % initial gas temperature in K, only used if not given in IC file

% Note: IGM temperature stays coupled to CMB temperature until
% thermalization redshift: z_t = 750 * (Omega_b * h^2) ~ 150

MinGasTemp          5.0
CourantFac          0.15

% Further code parameters

PartAllocFactor       1.5
%TreeAllocFactor      0.75

BufferSize            100

% System of units

UnitLength_in_cm     3.085678e24        ;  1.0 Mpc
UnitMass_in_g      1.989e43           ;  1.0e10 solar masses
UnitVelocity_in_cm_per_s 1e5                ;  1 km/sec
GravityConstantInternal  0

%---- Gravitational softening length
SofteningComovingClass0     600.0
SofteningMaxPhysClass0      600.0

SofteningClassOfPartType0    0
SofteningClassOfPartType1    0


%----- SPH
ArtBulkViscConst       1.0
MinEgySpec             0
InitGasTemp            1000.0    % in K

"""

        print("Done...")
        return txt

    def gen_param_gadget_file(self):
        print("Generate Gadget parameter file...")

        txt = "%  Relevant files\n"
        txt += "InitCondFile     " + self.out_dir + "/ic/ic.gad \n"
        txt += "OutputDir     " + self.out_dir + "/resim/outputs\n"
        txt += "EnergyFile     " + "energy.txt\n"
        txt += "InfoFile     " + "info.txt\n"
        txt += "TimingsFile     " + "timings.txt\n"
        txt += "CpuFile     " + "cpu.txt\n"
        txt += "TimebinFile     " + "timebin.txt\n"
        txt += "SnapshotFileBase     " + "snap_borg\n"
        txt += "RestartFile     " + "restart\n"
        txt += "% CPU-time limit     \n"
        txt += "MaxMemSize     " + "4096\n"
        txt += "TimeLimitCPU     " + "36000\n"

        txt += "TimeLimitCPU      " + "36000\n"
        txt += "ResubmitOn        " + "0\n"
        txt += "ResubmitCommand   " + "/home/vspringe/autosubmit\n"

        txt += "% Code options\n"

        # choose Gadget2 format consistently
        txt += "ICFormat              " + "2\n"
        txt += "SnapFormat            " + "3\n"

        txt += "ComovingIntegrationOn " + "1\n"

        txt += "NumFilesPerSnapshot       " + "1\n"
        txt += "NumFilesWrittenInParallel " + "2\n"

        txt += "CoolingOn       " + "0\n"
        txt += "StarformationOn " + "0\n"

        txt += "%  Caracteristics of run\n"

        # calculate initial scale factor
        txt += "TimeBegin           " + str(1.0 / (1.0 + self.zstart)) + "\n"
        txt += "TimeMax                " + "1.0     % end at z=0\n"

        txt += (
            "Omega0                "
            + str(self.cosmo["Omega_m"])
            + "% total matter density\n"
        )
        txt += "OmegaLambda           " + str(self.cosmo["Omega_L"]) + "\n"

        txt += "OmegaBaryon           " + str(self.cosmo["Omega_b"]) + "\n"
        txt += "HubbleParam           " + str(self.cosmo["H0"]) + "\n"

        if (self.L0 == self.L1) * (self.L0 == self.L2):
            txt += "BoxSize                " + str(self.L0) + "\n"
        else:
            print("Error: cannot setup parameter file for non-cubic Domain...")
            print("Stopping: Preparing Gadget parameter file!")
            exit()

        txt += "PeriodicBoundariesOn   " + "1\n"

        txt += "% Softening lengths\n"

        txt += (
            "MinGasHsmlFractional     "
            + "0.1  % minimum gas smoothing in terms of the gravitational softening length\n"
        )

        txt += "SofteningGas       " + "260.0\n"
        txt += "SofteningHalo      " + "260.0\n"
        txt += "SofteningDisk      " + "0.0\n"
        txt += "SofteningBulge     " + "0.0\n"
        txt += "SofteningStars     " + "260.0\n"
        txt += (
            "SofteningBndry     "
            + "260.0         % this will be the black hole particles2\n"
        )

        txt += "SofteningGasMaxPhys       " + "260.0\n"
        txt += "SofteningHaloMaxPhys      " + "260.0\n"
        txt += "SofteningDiskMaxPhys      " + "0.0\n"
        txt += "SofteningBulgeMaxPhys     " + "0.0\n"
        txt += "SofteningStarsMaxPhys     " + "260.0\n"
        txt += "SofteningBndryMaxPhys     " + "260.0\n"

        txt += "% Output frequency\n"

        txt += "OutputListOn        " + "0\n"
        txt += "OutputListFilename  " + self.out_dir + "/outputlists/outputs.txt\n"

        txt += "TimeBetSnapshot        " + "1.1\n"
        txt += "TimeOfFirstSnapshot    " + "0.1\n"

        txt += "CpuTimeBetRestartFile  " + "7200.0  % every 2 hours\n"

        txt += "TimeBetStatistics      " + "0.5\n"

        txt += "MaxRMSDisplacementFac  " + "0.25\n"

        txt += "% Accuracy of time integration\n"

        txt += "TypeOfTimestepCriterion " + "0\n"

        txt += "ErrTolIntAccuracy       " + "0.05\n"

        txt += "MaxSizeTimestep        " + "0.05\n"
        txt += "MinSizeTimestep        " + "1.0e-7\n"

        txt += "% Tree algorithm and force accuracy\n"

        txt += "ErrTolTheta             " + "0.4\n"

        txt += "TypeOfOpeningCriterion  " + "1\n"
        txt += "ErrTolForceAcc          " + "0.005\n"

        txt += "TreeDomainUpdateFrequency    " + "0.1\n"

        txt += "%  Parameters of SPH"

        txt += "DesNumNgb           " + "33\n"
        txt += "MaxNumNgbDeviation  " + "2\n"

        txt += "ArtBulkViscConst    " + "1.0\n"

        txt += (
            "InitGasTemp         "
            + "145.0  % initial gas temperature in K, only used if not given in IC file\n"
        )

        txt += "% Note: IGM temperature stays coupled to CMB temperature until\n"
        txt += "% thermalization redshift: z_t = 750 * (Omega_b * h^2) ~ 150\n"

        txt += "MinGasTemp          " + "5.0\n"
        txt += "CourantFac          " + "0.15\n"

        txt += "% Further code parameters\n"

        txt += "PartAllocFactor       " + "1.5\n"
        txt += "%TreeAllocFactor       " + "0.75\n"

        txt += "BufferSize              " + "100\n"

        txt += "% System of units\n"

        txt += "UnitLength_in_cm         " + "3.085678e24        ;  1.0 Mpc\n"
        txt += (
            "UnitMass_in_g            " + "1.989e43           ;  1.0e10 solar masses\n"
        )
        txt += "UnitVelocity_in_cm_per_s " + "1e5                ;  1 km/sec\n"
        txt += "GravityConstantInternal  " + "0\n"

        print("Done...")
        return txt

    def find_ncpu(self, fbase):
        import glob

        nn = len(glob.glob(fbase + "*"))
        return nn

    def initiate_run_gadget(self):
        print("Initiating Gadget run...")

        # generate gadget parameter file
        self.gen_param_gadget_file()

        print("Done...")

    def initiate_run_ramse(self):
        print("Initiating RAMSES run...")

        # generate gadget parameter file
        # self.gen_param_gadget_file()
        print("Not implemented yet")

        print("Done...")

    def select(self, wn):
        print("Attention: Cutting out central region from BORG domain")

        N0 = self.N0
        N1 = self.N1
        N2 = self.N2

        self.L0 = self.L0 * 0.5
        self.L1 = self.L1 * 0.5
        self.L2 = self.L2 * 0.5
        self.N0 = self.N0 // 2
        self.N1 = self.N1 // 2
        self.N2 = self.N2 // 2

        return wn[N0 // 4 : N0 * 3 // 4, N1 // 4 : N1 * 3 // 4, N2 // 4 : N2 * 3 // 4]

    def generate_music_param_file(self):
        print("Generating Music parameter file...")

        print("Done...")

    def gen_whitenoise(self, sample_nr):
        # here we need to check for the number of cpus used for the BORG run
        with h5.File(
            os.path.join(self.path, f"{self.mcmc_file_base}{sample_nr}.h5"),
            mode="r",
        ) as f:
            try:
                # update cosmological parameters
                caux = f["scalars/cosmology"][0]
                self.retrieve_cosmology(caux)
            except:
                print(
                    "Sample does not provide cosmology update. Continue with fixed cosmology..."
                )

            s_hat = f["/scalars/s_hat_field"][:]
            if self.version == 1:
                Pk = f["/scalars/powerspectrum"][:]

        if self.version == 1:
            Pk = np.sqrt(Pk * self.volume)[self.key_modes]
            foo = np.where(Pk > 0)
            s_hat[foo] /= Pk[foo]
            s_hat[0, 0, 0] = 0

        x = np.fft.irfftn(s_hat)
        x *= np.sqrt(x.size)

        if self.select_inner_region:
            x = self.select(x)

        # plt.hist(x.flatten(),bins=200)
        # plt.show()

        # ct.writeWhitePhase(self.out_dir+'/white_noise/'+self.fname_white_noise, x)

        with open(
            f"{self.out_dir}/white_noise/{self.fname_white_noise}", mode="wb"
        ) as f:
            Nx, Ny, Nz = x.shape

            checkPoint = 4 * 4
            f.write(struct.pack("IIIIII", checkPoint, Nx, Ny, Nz, 0, checkPoint))

            x = x.reshape(x.shape, order="F")
            checkPoint = struct.pack("I", 4 * Ny * Nz)
            for i in range(Nx):
                f.write(checkPoint)
                f.write(x[i].astype(np.float32).tostring())
                f.write(checkPoint)

    def gen_ic(self, sample_nr=0):
        print("Generating initial conditions with MUSIC...")

        self.gen_whitenoise(sample_nr)

        with open(f"{self.out_dir}/white_noise/music_param.txt", "w") as text_file:
            text_file.write(self.gen_music_param_file())

        # execute music from white noise directory
        if self.simulator != "WHITE":
            p = subprocess.Popen(
                [self.MUSIC_EXECUTABLE, "music_param.txt"],
                cwd=self.out_dir + "/white_noise/",
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                universal_newlines=True,
            )
            for line in iter(p.stdout.readline, ""):
                # Print output to console
                print(line, end="")
            p.wait()

            if self.simulator == "Gadget":
                with open(self.out_dir + "/gadget_param.txt", "w") as text_file:
                    text_file.write(self.gen_param_gadget_file())
            elif self.simulator == "Gadget4":
                with open(self.out_dir + "/gadget_param.txt", "w") as text_file:
                    text_file.write(self.gen_param_gadget4_file())
            elif self.simulator == "RAMSES":
                copyfile(
                    self.out_dir + "/ic/ramses_ic/ramses.nml",
                    self.out_dir + "/ramses.nml",
                )

                # modify nml file
                s = open(self.out_dir + "/ramses.nml").read()

                txt = "&AMR_PARAMS\n"
                txt += (
                    "ngridmax="
                    + str(self.N0 * self.N1 * self.N2 * 8 ** (self.fac_res - 1) * 2)
                    + "\n"
                )
                txt += (
                    "npartmax="
                    + str(self.N0 * self.N1 * self.N2 * 8 ** (self.fac_res - 1) * 2)
                    + ""
                )

                s = s.replace("&AMR_PARAMS", txt)
                f = open(self.out_dir + "/ramses.nml", "w")
                f.write(s)
                f.close()

                f = open(self.out_dir + "/ramses.nml", "a")
                txt = "&OUTPUT_PARAMS\n"
                txt += "noutput=1\n"
                txt += "aout=1.\n"
                txt += "/ \n"

                f.write(txt)
                f.close()

        # print summary
        print(
            f"""IC generation report:
IC for BORG sample {sample_nr} has been generated ...
Parameter files for simulator: {self.simulator} has been created
Please find the ICs at: {self.out_dir}"""
        )
