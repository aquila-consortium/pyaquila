# +
# +
from .bcs import ConstrainedSimulation
import sys


def run():
    import argparse
    import shutil

    def find_music():
        return shutil.which("MUSIC")

    music = find_music()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--music",
        help="Path to music executable",
        default=music,
    )
    parser.add_argument(
        "--simulator",
        help="Which simulator to target (Gadget,RAMSES,WHITE)",
        default="RAMSES",
    )
    parser.add_argument(
        "--sample",
        type=int,
        help="Which sample to consider",
        default=0,
    )
    parser.add_argument("--mcmc", help="Path of the MCMC chain", default=".")
    parser.add_argument("--output", help="Output directory", default=".")
    parser.add_argument(
        "--mcmc-version", help="Version of the MCMC (1 or 2)", default=2, type=int
    )
    parser.add_argument(
        "--augment", type=int, help="Factor by which to augment small scales", default=1
    )
    parser.add_argument(
        "--anti_sim",
        type=int,
        help="Create initial conditions with changed sign",
        default=0,
    )
    args = parser.parse_args()

    if args.simulator != "WHITE":
        if args.music is None:
            print(
                "MUSIC is not found in the path and has not been provided on the command line."
            )
            sys.exit(1)

    # set configuration for initial conditions generator
    genic = ConstrainedSimulation(
        fac_res=args.augment,
        out_dir=args.output,
        path=args.mcmc,
        which_simulator=args.simulator,
        MUSIC_EXECUTABLE=args.music,
        version=args.mcmc_version,
        anti_sim=args.anti_sim,
    )
    genic.gen_ic(sample_nr=args.sample)


__all__ = ["ConstrainedSimulation", "run"]
