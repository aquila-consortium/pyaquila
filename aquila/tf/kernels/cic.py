#
# CIC kernels
#
import tensorflow as tf

na = tf.newaxis


class CICProjection:
    """
    Compute the cloud-in-cell projection of a set of particles
    """

    tf_t = tf.float32

    def __init__(self, Lbox, Ng):
        """
        Arguments:
            - Lbox (float): box size in Mpc/h
            - Ng (int): grid mesh size
        """
        self.Lbox = tf.cast(Lbox, tf.float32)
        self.Ng = Ng
        self.Ng_f = tf.cast(Ng, self.tf_t)
        self.delta = self.Lbox / self.Ng_f

        q_base = tf.range(Ng, dtype=self.tf_t) * (Lbox / Ng) + 0.5
        self.qz = q_base[:, None, None]
        self.qy = q_base[None, :, None]
        self.qx = q_base[None, None, :]

        self.connection = tf.constant(
            [
                [
                    [0, 0, 0],
                    [1.0, 0, 0],
                    [0.0, 1, 0],
                    [0.0, 0, 1],
                    [1.0, 1, 0],
                    [1.0, 0, 1],
                    [0.0, 1, 1],
                    [1.0, 1, 1],
                ]
            ]
        )

    def build_q_field(self):
        QZ = tf.repeat(tf.repeat(self.qz, self.Ng, axis=1), self.Ng, axis=2)
        QY = tf.repeat(tf.repeat(self.qy, self.Ng, axis=0), self.Ng, axis=2)
        QX = tf.repeat(tf.repeat(self.qx, self.Ng, axis=0), self.Ng, axis=1)

        q_field = tf.transpose(tf.stack([QX, QY, QZ]), perm=[1, 2, 3, 0])
        return q_field[na, ...]

    def build_x_position(self, displacement):
        q_field = self.build_q_field()
        return displacement + q_field

    def project(self, positions):
        """compute the CIC projection based on displacement

        Args:
            displacement (tf.Tensor): BxNx3  (B = batch size, N number of particles)

        Returns:
            tf.Tensor: BxNxNxN
        """
        batch_size = tf.shape(positions)[0]
        nc = int(positions.get_shape()[1])

        part = tf.reshape(positions, (batch_size, nc**3, 3))

        # Extract the indices of all the mesh points affected by each particles
        part = part[..., na] * self.delta
        i_part = tf.floor(part)
        connection = self.connection[na, na, :]
        neighboor_coords = i_part + connection
        kernel = 1.0 - tf.abs(part - neighboor_coords)
        neighboor_coords = tf.cast(neighboor_coords, tf.int32) % self.Ng

        kernel = kernel[..., 0] * kernel[..., 1] * kernel[..., 2]

        # Adding batch dimension to the neighbour coordinates
        batch_idx = tf.range(0, batch_size)[:, na, na, na]
        b = tf.tile(batch_idx, [1] + list(neighboor_coords.get_shape()[1:-1]) + [1])

        neighboor_coords = tf.concat([b, neighboor_coords], axis=-1)
        CIC_field = tf.scatter_nd(
            tf.reshape(neighboor_coords, (-1, 8, 4)),
            tf.reshape(kernel, (-1, 8)),
            [batch_size, nc, nc, nc],
        )

        return CIC_field
