"""Module to handle data augmentation based on physical symmetries

Original code from Tom Charnock & Doogesh Kodi Ramanah (2019)
"""


import tensorflow as tf


@tf.function
def spatial_invariance(symmetry, x):
    """Apply the given symmetry to x.

    This function is compiled with tensorflow XLA translator.

    Args:
        symmetry (int): An integer between 0 and 23 (included)
        x (tf.Tensor): a tensor with shape BxNxNxNxP

    Returns:
        tf.Tensor: The x tensor with symmetry applied

    Examples:
        Here is an example of usage of this function

        >>> x = g.uniform((1, 8, 8, 8, 1), dtype=tf.float32)
        >>> z = g.uniform((1000, ), minval=0, maxval=23, dtype=tf.int32)
        >>> r = tf.map_fn(lambda a: spatial_invariance(a, x)[0, ...],
              z,
              fn_output_signature=tf.float32)
    """

    c = symmetry

    if c == 0:
        return x
    elif c == 1:
        return x[:, ::-1, ::-1, :, :]
    elif c == 2:
        return x[:, ::-1, :, ::-1, :]
    elif c == 3:
        return x[:, :, ::-1, ::-1, :]
    elif c == 4:
        return tf.transpose(x, (0, 2, 1, 3, 4))[:, ::-1, :, :, :]
    elif c == 5:
        return tf.transpose(x, (0, 2, 1, 3, 4))[:, ::-1, :, ::-1, :]
    elif c == 6:
        return tf.transpose(x, (0, 2, 1, 3, 4))[:, :, ::-1, :, :]
    elif c == 7:
        return tf.transpose(x, (0, 2, 1, 3, 4))[:, :, ::-1, ::-1, :]
    elif c == 8:
        return tf.transpose(x, (0, 3, 2, 1, 4))[:, ::-1, :, :, :]
    elif c == 9:
        return tf.transpose(x, (0, 3, 2, 1, 4))[:, ::-1, ::-1, :, :]
    elif c == 10:
        return tf.transpose(x, (0, 3, 2, 1, 4))[:, :, :, ::-1, :]
    elif c == 11:
        return tf.transpose(x, (0, 3, 2, 1, 4))[:, :, ::-1, ::-1, :]
    elif c == 12:
        return tf.transpose(x, (0, 1, 3, 2, 4))[:, :, ::-1, :, :]
    elif c == 13:
        return tf.transpose(x, (0, 1, 3, 2, 4))[:, ::-1, ::-1, :, :]
    elif c == 14:
        return tf.transpose(x, (0, 1, 3, 2, 4))[:, :, :, ::-1, :]
    elif c == 15:
        return tf.transpose(x, (0, 1, 3, 2, 4))[:, ::-1, :, ::-1, :]
    elif c == 16:
        return tf.transpose(x, (0, 2, 3, 1, 4))[:, ::-1, ::-1, :, :]
    elif c == 17:
        return tf.transpose(x, (0, 2, 3, 1, 4))[:, :, ::-1, ::-1, :]
    elif c == 18:
        return tf.transpose(x, (0, 2, 3, 1, 4))[:, ::-1, :, ::-1, :]
    elif c == 19:
        return tf.transpose(x, (0, 2, 3, 1, 4))[:, ::-1, ::-1, ::-1, :]
    elif c == 20:
        return tf.transpose(x, (0, 3, 1, 2, 4))[:, ::-1, ::-1, :, :]
    elif c == 21:
        return tf.transpose(x, (0, 3, 1, 2, 4))[:, ::-1, :, ::-1, :]
    elif c == 22:
        return tf.transpose(x, (0, 3, 1, 2, 4))[:, :, ::-1, ::-1, :]
    elif c == 23:
        return tf.transpose(x, (0, 3, 1, 2, 4))[:, ::-1, ::-1, ::-1, :]
    else:
        return x
