# Part of the code has been written by D. K. Ramanah
# Some other part by G. Lavaux
import tensorflow as tf


def compute_divergence_field(input_disp_field, periodic=False):
    """
    We use displacement field and compute the centered, finite difference, divergence.

    Arguments:
        - input_disp_field (tf.Tensor): a tensor NxNxNx3
        - N (int): resolution

    Returns:
        - tf.Tensor, a tensor NxNxN
    """

    if periodic:
        div_disp = None
        for j in range(3):
            a = tf.roll(input_disp_field[:, :, :, j], shift=1, axis=j) - tf.roll(
                input_disp_field[:, :, :, j], shift=-1, axis=j
            )
            if div_disp is None:
                div_disp = a
            else:
                div_disp = a + div_disp
        div_disp = 0.5 * div_disp
    else:
        div_disp = 0.5 * (
            (
                input_disp_field[2:, 1:-1, 1:-1, 0]
                - input_disp_field[0:-2, 1:-1, 1:-1, 0]
            )
            + (
                input_disp_field[1:-1, 2:, 1:-1, 0]
                - input_disp_field[1:-1, 0:-2, 1:-1, 0]
            )
            + (
                input_disp_field[1:-1, 1:-1, 2:, 0]
                - input_disp_field[1:-1, 1:-1, 0:-2, 0]
            )
        )
    return div_disp


def compute_gradient_field(field, periodic=False):
    """
    Compute the gradient (NxNxNx3) of the provided scalar field (NxNxN)

    Arguments:
        - field (tf.Tensor): NxNxN

    Returns:
        tf.Tensor: gradient, NxNxNx3
    """

    if periodic:
        g = []
        for j in range(3):
            g.append(
                0.5
                * (tf.roll(field, shift=1, axis=j) - tf.roll(field, shift=-1, axis=j))
            )
    else:
        g = []
        g.append(0.5 * (field[2:, 1:-1, 1:-1] - field[0:-2, 1:-1, 1:-1]))
        g.append(0.5 * (field[1:-1, 2:, 1:-1] - field[1:-1, 0:-2, 1:-1]))
        g.append(0.5 * (field[1:-1, 1:-1, 2:] - field[1:-1, 1:-1, 0:-2]))

    return tf.stack(g)
