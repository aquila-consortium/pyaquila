"""Module to implement a U-Net with 3D input
"""

import tensorflow as tf
import numpy as np
from aquila import logger, log_context


class Conv3D(tf.Module):
    """Generic kernel for a conv3D layer with standard arguments

    Construct a new 3d convolution

    Arguments:
        name (str): prefix name of the variables
        kernel_size (int): size of the convolution
        filters (int): number of different filtered output

    Keyword arguments:
        x_in (tf.Tensor): Either x_in or input_filters must be specified
        input_filters (int): Number of filters input to the general convolution
    """

    def __init__(self, name, kernel_size, filters, x_in=None, input_filters=None):
        super(Conv3D, self).__init__()
        with log_context(f"Conv3d(in={input_filters},out={filters})"):
            if not x_in is None:
                input_filters = x_in.get_shape().as_list()[-1]
            else:
                assert not input_filters is None

            # logger.debug(f"filters={filters}")
            initer = tf.random_normal_initializer(mean=0, stddev=0.1)
            initer = initer(
                shape=(kernel_size, kernel_size, kernel_size, input_filters, filters)
            )
            # logger.debug(f"initer={initer}")
            self.w = tf.Variable(
                name=f"{name}_w", dtype=tf.float32, initial_value=initer
            )
            # logger.debug(f"w={self.w}")

            self.b = tf.Variable(
                name=f"{name}_b",
                initial_value=tf.constant_initializer(0.1)(
                    shape=(filters,), dtype=tf.float32
                ),
            )
            # logger.debug(f"b={self.b}")

            self.input_filters = input_filters
            self.filters = filters

    @tf.function
    def __call__(self, x_in, stride, padding):
        """Execute the convolution.

        Arguments:
            x_in (tf.Tensor): the input 3d mesh
            stride (int):
            padding (int):
        """
        x = tf.nn.conv3d(
            input=x_in,
            filters=self.w,
            strides=[1, stride, stride, stride, 1],
            padding=padding,
        )

        x = x + self.b

        return x

    def output_filters(self):
        """Return the number of allocated output filters

        Returns:
            int: number of output filters
        """
        return self.filters

    def __repr__(self):
        return f"<Conv3D: input_filters={self.input_filters}, filters={self.filters}>"


class Conv3DTranspose(tf.Module):
    """Generic kernel for a transposed conv3D layer with standard arguments

    Construct a new 3d transposed convolution

    Arguments:
        name (str): prefix name of the variables
        kernel_size (int): size of the convolution
        filters (int): number of different filtered output

    Keyword arguments:
        x_in (tf.Tensor): Either x_in or input_filters must be specified
        input_filters (int): Number of filters input to the general convolution
    """

    def __init__(self, name, kernel_size, filters, x_in=None, input_filters=None):
        super(Conv3DTranspose, self).__init__()
        with log_context(f"Conv3dTranspose(in={input_filters},out={filters})"):
            if not x_in is None:
                input_filters = x_in.get_shape().as_list()[-1]
            else:
                assert not input_filters is None

            self.input_filters = input_filters
            self.filters = filters

            logger.debug(f"Filters={filters}")
            self.w = tf.Variable(
                name=f"{name}_w",
                dtype=tf.float32,
                initial_value=tf.random_normal_initializer(mean=0, stddev=0.1)(
                    shape=(
                        (kernel_size, kernel_size, kernel_size, filters, input_filters)
                    )
                ),
            )
            # logger.debug(f"w={self.w}")

            self.b = tf.Variable(
                name=f"{name}_b",
                initial_value=tf.constant_initializer(0.1)(
                    shape=(filters,), dtype=tf.float32
                ),
            )

    def __call__(self, x_in, stride, padding):
        batch_size = tf.shape(x_in)[0]
        slice_size = x_in.get_shape().as_list()[-2]
        x = (
            tf.nn.conv3d_transpose(
                x_in,
                self.w,
                output_shape=[
                    batch_size,
                    int(stride * slice_size),
                    int(stride * slice_size),
                    int(stride * slice_size),
                    self.filters,
                ],
                strides=[1, stride, stride, stride, 1],
                padding=padding,
            )
            + self.b
        )

        return x

    def output_filters(self):
        """Return the number of allocated output filters

        Returns:
            int: number of output filters
        """
        return self.filters

    def __repr__(self):
        return f"<Conv3DTranspose: input_filters={self.input_filters}, filters={self.filters}>"


class UNet(tf.Module):
    """The U-Net has a symmetric encoder-decoder convolutional structure,
       with a contraction path that downsamples the input, halving the
       spatial dimensionality while simultaneously doubling the number of
       filters at each level, through "n_steps" levels, to reach a bottleneck
       and a subsequent expansion path that gradually upsamples the compressed
       representation to ultimately yield an output of same dimensions as
       the network input.
       The downsampling is done via a strided convolution (i.e. stride=2),
       whilst the upsampling proceeds via a transposed convolution.
       Mirrored skip connections concatenate features at each depth.

    Args:
        x_in (tf.Tensor): a tensor with shape BxNxNxNxn_in, where n_in
                          dictates whether scalar or vector field input
        n_steps (int): an integer to specify number of levels in the U-Net,
                       For e.g., a three-level U-Net with 64³ cubes as
                       input will process the dimensions as
                       64³→32³→16³→8³→16³→32³→64³
        conv_kernel(int): size of 3D kernel for all conv3D layers
        n_filters(int): starting number of feature channels
        n_out(int): number of filters for output. For e.g.,
                    n_out=1 for a scalar field (e.g. density field)
                    n_out=3 for a vector field (e.g. displacement field)
        leaky_param(float): slope for Leaky ReLU activation

    Returns:
        tf.Tensor: A tensor of same spatial dimensionality as the input,
                   with shape BxNxNxNxn_out
    """

    def __init__(
        self,
        input_filters,
        n_steps=3,
        conv_kernel=3,
        n_filters=8,
        n_out=1,
        leaky_param=0.2,
    ):
        super(UNet, self).__init__()
        with log_context(
            f"UNet3d(input_filters={input_filters}, n_steps={n_steps}, conv_kernel={conv_kernel}, n_filters={n_filters}, n_out={n_out}, leaky_param={leaky_param})"
        ):

            self.n_steps = n_steps

            self.input_conv = Conv3D(
                kernel_size=conv_kernel,
                filters=int(n_filters * 1),
                input_filters=input_filters,
                name="layer_in",
            )

            previous_output_filters = self.input_conv.output_filters()
            logger.debug(f"previous_output_filters={previous_output_filters}")
            steps = range(1, self.n_steps + 1)
            self.down_convs = []
            for k in steps:
                new_conv = Conv3D(
                    input_filters=previous_output_filters,
                    kernel_size=conv_kernel,
                    filters=int(n_filters * 2**k),
                    name=f"layer_{k}a",
                )

                new_conv2 = Conv3D(
                    input_filters=new_conv.output_filters(),
                    kernel_size=conv_kernel,
                    filters=int(n_filters * 2**k),
                    name=f"layer_{k}b",
                )
                new_conv3 = Conv3D(
                    input_filters=new_conv2.output_filters(),
                    kernel_size=conv_kernel,
                    filters=int(n_filters * 2**k),
                    name=f"layer_{k}c",
                )

                previous_output_filters = new_conv3.output_filters()
                self.down_convs.append((new_conv, new_conv2, new_conv3))

            self.up_convs = []
            for k in steps[::-1]:
                c1 = Conv3DTranspose(
                    input_filters=previous_output_filters,
                    kernel_size=conv_kernel,
                    filters=int(n_filters * 2 ** (k - 1)),
                    name=f"layer_{k}a_m",
                )

                if k >= 2:
                    output = self.down_convs[k - 2][2].output_filters()
                else:
                    output = self.input_conv.output_filters()

                logger.debug(f"Stacking (k={k}) --- {c1.output_filters()}, {output}")
                c2 = Conv3D(
                    input_filters=(c1.output_filters() + output),
                    kernel_size=conv_kernel,
                    filters=int(n_filters * 2 ** (k - 1)),
                    name=f"layer_{k}b_m",
                )

                c3 = Conv3D(
                    input_filters=c2.output_filters(),
                    kernel_size=conv_kernel,
                    filters=int(n_filters * 2 ** (k - 1)),
                    name=f"layer_{k}c_m",
                )

                previous_output_filters = c3.output_filters()

                self.up_convs.append((c1, c2, c3))

            self.up_convs = self.up_convs[::-1]

            self.output_conv = Conv3DTranspose(
                input_filters=previous_output_filters,
                kernel_size=1,
                filters=n_out,
                name="layer_out",
            )

    @tf.function
    def __call__(self, x_in, leaky_param):
        # Perform one (or two) Conv3D operations on the input
        # logger.debug(f"Preliminary convolution on {x_in}")
        x_ = tf.nn.leaky_relu(
            self.input_conv(x_in, stride=1, padding="SAME"), leaky_param
        )
        # logger.debug(x_)

        steps = range(1, self.n_steps + 1)
        # logger.debug("Encoding (contraction) path")
        down_layers = []
        for k in steps:
            down_layers.append(x_)
            c1, c2, c3 = self.down_convs[k - 1]
            # Downsample with Conv3D (stride=2)
            x_ = tf.nn.leaky_relu(c1(x_in=x_, stride=2, padding="SAME"), leaky_param)
            # logger.debug(x_)
            # Perform a couple of Conv3D operations on the downsampled representation
            x_ = tf.nn.leaky_relu(c2(x_in=x_, stride=1, padding="SAME"), leaky_param)
            # logger.debug(x_)
            x_ = tf.nn.leaky_relu(c3(x_in=x_, stride=1, padding="SAME"), leaky_param)
            # logger.debug(x_)

        # logger.debug(
        #    "Decoding (expansion) path")  # 'm' suffix denotes mirrored path
        # Reverse order of steps
        steps = steps[::-1]
        # print(f"Downward layers = {down_layers}, last layer = {x_}")
        for k in steps:
            c1, c2, c3 = self.up_convs[k - 1]
            # print(f"Convolution for going up={self.up_convs[k-1]}")
            # Upsample with Conv3DTranspose (stride=2)
            x_ = tf.nn.leaky_relu(c1(x_in=x_, stride=2, padding="SAME"), leaky_param)
            # print(f"Post-Adj-Conv : x={x_}")
            # Apply mirrored skip connection
            x_ = tf.concat((x_, down_layers[k - 1]), axis=4)
            # Perform a couple of Conv3D operations on the upsampled representation
            x_ = tf.nn.leaky_relu(c2(x_in=x_, stride=1, padding="SAME"), leaky_param)
            # logger.debug(x_)
            x_ = tf.nn.leaky_relu(c3(x_in=x_, stride=1, padding="SAME"), leaky_param)
            # logger.debug(x_)

        # For output layer, use a Conv3DTranspose (kernel=1, stride=1, filters=n_out)
        x_out = tf.nn.leaky_relu(
            self.output_conv(x_in=x_, stride=1, padding="SAME"), leaky_param
        )
        # logger.debug("Output =", x_out)

        return x_out
