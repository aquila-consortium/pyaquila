try:
    import tensorflow as tf

    from . import kernels, algorithms
except Exception as e:
    import traceback

    print(f"An exception occurred while loading Tensorflow: {e}")
    traceback.print_exc()
    print("Tensorflow kernels not available")
