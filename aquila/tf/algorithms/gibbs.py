"""Gibbs Sampling for Tensorflow

The Gibbs sampling algorithm is implemented using Tensorflow 2.x API. It is not super efficient but allow to check
the relative performance with respect to Hamiltonian Monte Carlo. It has also better stability guarantee in case the mass matrix
is bad in the HMC.

"""
import borg
import tensorflow as tf
import cosmotool as ct
import numpy as np
import tensorflow_probability as tfp
import collections

__all__ = ["InnerGibbs", "Gibbs"]


class InnerGibbs(
    collections.namedtuple("InnerGibbs", ["inner_results", "other_results"])
):
    """This is a named tuple to represent the hierarchy of results of the Gibbs Transition kernel"""

    __slots__ = ()


class Gibbs(tfp.mcmc.TransitionKernel):
    """Gibbs sampling algorithm

    Args:
        target_log_prob_fn (object):
        make_kernel_fns (List[tf.mcmc.TransitionKernel]):
        dim_slices (List[int]):
    """

    def __init__(self, target_log_prob_fn, make_kernel_fns, dim_slices):
        self._target_log_prob_fn = target_log_prob_fn
        self._make_kernel_fns = make_kernel_fns
        self._dim_slice = dim_slices

    def is_calibrated(self):
        """Part of Tensorflow API. Indicate that no Metropolis-Hasting step is necessary."""
        return True

    def _make_target(self, new_state, state, previous, next_piece):
        """Build a sub log-probability from the current progression in Gibbs sampling
        and the requested next sample to compute.

        The returned function is p-dimensional probability, with q+p+r = n, q
        the previous block sampling state, r the remaining of the space to be
        sampled after that one.
        """

        def _target_log_prob_fn_part(state_part):
            state_new = tf.concat(
                new_state + [state_part, tf.stop_gradient(state[next_piece:])], 0
            )
            return self._target_log_prob_fn(state_new)

        return _target_log_prob_fn_part

    def one_step(self, state, _):
        """Execute a full one loop of Gibbs sampling.

        This function is part of TF API.
        """
        previous = 0
        results = []
        new_state = []
        # We loop for each piece of Gibbs sampling that is requested
        for i, make_kernel_fn in enumerate(self._make_kernel_fns):
            next_piece = previous + self._dim_slice[i]
            # If the kernel is None, we just freeze the subspace in place
            if make_kernel_fn is None:
                new_state.append(state[previous:next_piece])
                results.append(())
            else:
                # Make the kernel
                kernel = make_kernel_fn(
                    self._make_target(new_state, state, previous, next_piece)
                )
                # Execute it once
                new_, r_ = kernel.one_step(
                    state[previous:next_piece],
                    kernel.bootstrap_results(state[previous:next_piece]),
                )
                # Store result
                new_state.append(new_)
                results.append(r_)
            previous = next_piece
        # Store the intermediate steps for later analysis
        r = InnerGibbs(inner_results=results[0], other_results=results)
        return tf.concat(new_state, 0), r

    def bootstrap_results(self, state):
        """Start the state from a sane initial condition."""
        results = []
        previous = 0
        for i, make_kernel_fn in enumerate(self._make_kernel_fns):
            next_piece = previous + self._dim_slice[i]
            if make_kernel_fn is None:
                results.append(())
            else:
                kernel = make_kernel_fn(
                    self._make_target([state[:previous]], state, previous, next_piece)
                )
                r_ = kernel.bootstrap_results(state[previous:next_piece])
                results.append(r_)
            previous = next_piece
        r = InnerGibbs(inner_results=results[0], other_results=results)
        return r
