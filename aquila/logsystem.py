import logging
import contextlib
import threading
from functools import wraps

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

# The background is set with 40 plus the number of the color, and the foreground with 30

# These are the sequences need to get colored ouput
RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[1;%dm"
BOLD_SEQ = "\033[1m"


def formatter_message(message, use_color=True):
    if use_color:
        message = message.replace("$RESET", RESET_SEQ).replace("$BOLD", BOLD_SEQ)
    else:
        message = message.replace("$RESET", "").replace("$BOLD", "")
    return message


COLORS = {
    "WARNING": YELLOW,
    "INFO": WHITE,
    "DEBUG": BLUE,
    "CRITICAL": YELLOW,
    "ERROR": RED,
}


indentLevel = threading.local()
indentLevel.val = 0
workerInfo = threading.local()
workerInfo.val = False


class ColoredFormatter(logging.Formatter):
    def __init__(self, msg, use_color=True):
        logging.Formatter.__init__(self, msg)
        self.use_color = use_color

    def format(self, record):
        global indentLevel
        levelname = record.levelname
        if self.use_color and levelname in COLORS:
            levelname_color = (
                COLOR_SEQ % (30 + COLORS[levelname]) + levelname + RESET_SEQ
            )
            record.levelname = levelname_color
        if hasattr(indentLevel, "val"):
            record.indent = " " * indentLevel.val
        else:
            record.indent = ""
        return logging.Formatter.format(self, record)


logger = logging.getLogger("aquila")
logger.setLevel(logging.DEBUG)

# create formatter
msg = formatter_message(
    "[%(levelname)s,($BOLD%(filename)20s$RESET:%(lineno)4d)] - %(indent)s %(message)s"
)
formatter = ColoredFormatter(msg)

console = logging.StreamHandler()
console.setFormatter(formatter)
logger.addHandler(console)


@contextlib.contextmanager
def _worker_context():
    global workerInfo
    if not hasattr(workerInfo, "val"):
        workerInfo.val = True
        previous = False
    else:
        previous = workerInfo.val
    yield
    workerInfo.val = previous


def in_worker(f):
    @wraps(f)
    def _wrapper(*args, **kwargs):
        with _worker_context():
            return f(*args, **kwargs)

    return _wrapper


@contextlib.contextmanager
def log_context(msg):
    global indentLevel, workerInfo
    if not hasattr(workerInfo, "val"):
        workerInfo.val = False
    if workerInfo.val:
        yield
        return
    logger.debug(f"Entering '{msg}'")
    if not hasattr(indentLevel, "val"):
        indentLevel.val = 0
    indentLevel.val += 4
    yield
    indentLevel.val -= 4
    logger.debug(f"Leaving '{msg}'")


def log_function(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if hasattr(f, "__qualname__"):
            f_name = f.__qualname__
        else:
            f_name = f.__name__
        with log_context(f_name + "@" + f.__module__):
            return f(*args, **kwargs)

    return wrapper


__all__ = ["logger", "log_context", "in_worker"]
