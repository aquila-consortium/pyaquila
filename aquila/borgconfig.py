import configparser
from pathlib import Path
from functools import cached_property
import aquila_borg as borg


class BORGConfig:
    """This is a Hades/BORG ini file parser.

    It helps at parsing the ini file and instancing Aquila_borg objects
    from the instructions contained within. It allows the user to
    safely reprocess MCMC chains with exactly the same setting.

    Arguments:
      * path (Path): path to the ini file
    """

    def __init__(self, path: Path):
        self._parser = configparser.ConfigParser()
        self._parser.read(path)

    @property
    def config(self):
        return self._parser

    @cached_property
    def cosmology(self):
        cpar = borg.cosmo.CosmologicalParameters()
        for c in self._parser["cosmology"]:
            if hasattr(cpar, c):
                setattr(cpar, c, float(self._parser["cosmology"][c]))
        return cpar

    @cached_property
    def ic_box(self):
        bbox = borg.forward.BoxModel()
        bbox.N = tuple(int(self._parser["system"][f"N{a}"]) for a in range(3))
        bbox.L = tuple(float(self._parser["system"][f"L{a}"]) for a in range(3))
        bbox.xmin = tuple(float(self._parser["system"][f"corner{a}"]) for a in range(3))
        return bbox

    def build_gravity_model(self):
        dynamic_model = None

        model_list = self._parser["gravity"]["models"].split(",")

        chain = borg.forward.ChainForwardModel(self.ic_box)
        bbox = self.ic_box
        for a, i in enumerate(model_list):
            opts = dict(self._parser[f"gravity_chain_{a}"])
            m = borg.forward.models.newModel(i, bbox, opts=opts)
            bbox = m.getOutputBoxModel()
            if "name" in opts:
                m.setName(opts["name"])
                if opts["name"] == "dynamics":
                    dynamics_model = m
            chain @= m
            if dynamic_model is None and isinstance(
                m, borg.forward.ParticleBasedForwardModel
            ):
                dynamic_model = m

        return chain, dynamic_model
