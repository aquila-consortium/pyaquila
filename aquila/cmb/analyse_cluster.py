import numpy as np
import healpy as hp


class analyse_sz:
    def __init__(self, y_mu, y_var, psi, mask, borg_clusters):

        self.y_mu = y_mu
        self.nside = hp.npix2nside(len(y_mu))
        self.y_var = y_var
        self.psi = psi
        self.npower = len(psi)
        self.mask = mask
        self.borg_clusters = borg_clusters
        self.ncluster = (np.max(borg_clusters) + 1).astype(int)

        self.description = "This class analyses the SZ effect"
        self.author = "Jens Jasche"

    def regrid(self, infield, range=[0, 0, 0, 0], N=1000):
        # This routine regrids data to a local cartesian grid
        y = np.linspace(range[0], range[1], N)
        x = np.linspace(range[2], range[3], N)
        X, Y = np.meshgrid(X, Y)

        ids = hp.ang2pix(self.nside, Y, X)

        outfield = infield[ids]
