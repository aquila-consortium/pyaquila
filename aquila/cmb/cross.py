import copy
import tqdm
import numpy as np
import healpy as hp
import numpy.typing as npt
import pprint
from typing import List, Union
from pathlib import Path
from functools import cache, partial
from ..logsystem import log_function, logger, in_worker

# from ..caching import cached
import dask.array as da
from dask import delayed


class CMBData:
    """Hold CMB data with mask."""

    _mask = None
    data = None

    def __init__(self, mask: npt.ArrayLike, data: npt.ArrayLike):
        self._mask = mask
        self.data = data
        if mask.size != data.size:
            raise ValueError(
                f"Mask and data do not have same size ({mask.size} != {data.size}"
            )

    @property
    def mask(self) -> npt.ArrayLike:
        return self._mask

    @property
    def Nside(self) -> int:
        return hp.npix2nside(self.data.size)

    def copy(self):
        return copy.deep_copy(self)

    def upgrade(nside_out):
        pass


class PlanckLensingData(CMBData):
    def __init__(self, mask: npt.ArrayLike, data: npt.ArrayLike):
        super().__init__(mask, data)

    @classmethod
    @log_function
    def from_lensing_directory_v3(
        cls,
        planck_directory: Union[str, Path],
        estimator: str,
        coord: str = None,
        lmax: int = None,
    ):
        """Data loader for planck map v3.00

        Args:
          * planck_directory (Path): base path for the lensing maps
          * estimator (str): estimator to consider

        Returns (PlanckLensingData):
          loaded data and constructed CMBData
        """
        from astropy.io import fits

        if type(planck_directory) is str:
            # promote the path
            planck_directory = Path(planck_directory)

        klm = hp.read_alm(planck_directory / estimator / "dat_klm.fits")
        mask = hp.read_map(planck_directory / "mask.fits.gz")

        initial_lmax = hp.Alm.getlmax(klm.size)
        if lmax is not None and initial_lmax != lmax:
            new_size = hp.Alm.getsize(lmax)
            l, m = hp.Alm.getlm(lmax, np.arange(new_size))
            lm_idx = hp.Alm.getidx(initial_lmax, l, m)
            klm = klm[lm_idx]
        if coord is not None:
            r = hp.rotator.Rotator(coord="G" + coord)
            r.rotate_alm(klm, inplace=True)
            mask = r.rotate_map_pixel(mask)
        Nside = hp.npix2nside(mask.size)
        data = hp.alm2map(klm, nside=Nside, lmax=lmax)
        return cls(mask, data)


class KappaMCData:
    _Nside = None

    def __iter__(self):
        raise NotImplementedError("iterators not implemented")

    def __next__(self):
        raise NotImplementedError("iterators not implemented")

    def progress(self):
        return tqdm.tqdm(self)

    @property
    def Nside(self) -> int:
        return self._Nside


class BORGKappa(KappaMCData):
    @log_function
    def __init__(
        self,
        path: Union[str, Path] = None,
        info: tuple = None,
        start: int = 0,
        end: int = -1,
        pattern: str = "kappa_%d.npy",
        mask: str = None,
        upgrade=None,
    ):
        super().__init__()

        if info is not None:
            self.pattern, self.start, self.current, self.end, self.path = info
            self._mask = None
        else:
            assert path is not None

            self.pattern = pattern
            self.start = start
            self.end = end
            if type(path) is str:
                path = Path(path)
            self.path = path
            self._mask = mask
            if upgrade is not None:
                self._mask = hp.ud_grade(self._mask, nside_out=upgrade)
            self.current = -1

    @property
    def info(self) -> tuple:
        return self.pattern, self.start, self.current, self.end, self.path

    @property
    def mask(self) -> npt.ArrayLike:
        return self._mask

    def upgrade_mask(self, Nside):
        self._mask = hp.ud_grade(self._mask, nside_out=Nside)

    @property
    def data(self) -> npt.ArrayLike:
        self._cache_data()
        return self._data

    def __len__(self) -> int:
        return self.end - self.start

    def __iter__(self):
        self.current = self.start
        return self

    def __getitem__(self, index):
        newself = copy.copy(self)
        newself.current = self.start + index
        newself._data = None
        newself._cache_data()
        return newself

    def _cache_data(self) -> None:
        if self._data is not None:
            return
        self._data = np.load(self.path / (self.pattern % self.current))

    def upgrade(self, Nside):
        self._cache_data()
        self._data = hp.ud_grade(self._data, nside_out=Nside)

    def __next__(self):
        if self.current == self.end or self.current == -1:
            raise StopIteration()
        self.current += 1
        self._data = None
        return self

    def __repr__(self):
        return (
            f"<BORGKappa: current={self.current}, start={self.start}, end={self.end}>"
        )


@delayed
@in_worker
def _load_and_upgrade(kappa_info, foo, Nside, index):
    kappa_data = BORGKappa(info=kappa_info)
    m = kappa_data[index]
    m.upgrade(Nside)
    return m.data[foo]


@delayed
@in_worker
def _make_histo(data, cmb_data, NumBins, kappa_min, kappa_max):
    # import gc
    #
    # a1 = np.empty((100,))#self.foo.size,))
    # a2 = cmb_foo

    borg_hist, bin_edges = np.histogram(
        data, bins=NumBins, range=[kappa_min, kappa_max]
    )
    ww1, bin_edges = np.histogram(
        data, weights=cmb_data, bins=NumBins, range=[kappa_min, kappa_max]
    )
    # del m
    # gc.collect()
    # return da.from_array(np.empty((NumBins,)))
    # return np.empty((NumBins,))

    return ww1 / (1e-6 + borg_hist.astype(float))


class LensingCrossCorrelation:
    @log_function
    def __init__(self, cmb_data: CMBData, kappa_data: KappaMCData):
        self.cmb_data = cmb_data
        self.kappa_data = kappa_data

        self.mask = self.kappa_data.mask * self.cmb_data.mask
        self._indexes = np.where(
            self.cmb_data.mask
        )  # foo = np.where(mask>0.0) #only take non-masked pixel
        self.foo = np.where(self.mask > 0)[0]

    @cache
    @log_function
    def compute(
        self, NumBins=20, kappa_min=-1e-2, kappa_max=1e-2
    ) -> list[npt.ArrayLike]:
        """Compute the 2d  cross-correlation of CMB data with BORG data

        Keyword Arguments:
           * NumBins (int, default 20): number of bins
           * kappa_min (float, default -1e-2): minimum kappa
           * kappa_max (float, default -1e-2): maximum kappa

        Returns: list of
        """
        res = []

        k_map = self.cmb_data.data
        all_results = []

        cmb_foo = self.cmb_data.data[self.foo]
        _, bin_edges = np.histogram(cmb_foo, bins=NumBins, range=[kappa_min, kappa_max])

        for i in range(len(self.kappa_data)):
            data = da.from_delayed(
                _load_and_upgrade(
                    self.kappa_data.info, self.foo, self.cmb_data.Nside, i
                ),
                shape=(self.foo.size,),
                dtype=np.float64,
            )
            result = da.from_delayed(
                _make_histo(data, cmb_foo, NumBins, kappa_min, kappa_max),
                shape=(NumBins,),
                dtype=np.float64,
            )
            all_results.append(result)
        res = da.stack(all_results, axis=0)

        return {"bins": bin_edges, "counts": res}

    @log_function
    def save_compute(self, **kwargs):
        ret = self.compute(**kwargs)
        ret["counts"] = ret["counts"].compute()
        np.save("compute_cross.npy", ret)

    @log_function
    def load_compute(self, **kwargs):
        self.pre_compute = np.load("compute_cross.npy")

    ###@partial(cached, skip_object=True)
    @log_function
    def average(self, **kwargs):
        ret = self.compute(**kwargs)
        return {
            "bins": ret["bins"],
            "num": len(ret["counts"]),
            "avg": da.average(ret["counts"], axis=0),
        }

    ##@partial(cached, skip_object=True)
    @log_function
    def std(self, **kwargs):
        ret = self.compute(**kwargs)
        return {
            "bins": ret["bins"],
            "num": len(ret["counts"]),
            "std": da.std(ret["counts"], axis=0),
        }

    @log_function
    def restore(self, filepath: Union[str, Path]):
        if type(filepath) is str:
            filepath = Path(filepath)
        try:
            # self.compute.restore(filepath)
            logger.debug("Restored cache")
        except IOError as e:
            pprint.pprint(e)
            pass

    @log_function
    def save(self, filepath: Union[str, Path]):
        if type(filepath) is str:
            filepath = Path(filepath)
        # self.compute.save(filepath)

    @log_function
    def plot(self, ax=None):
        import matplotlib.pyplot as plt
        from distributed import progress

        if ax is None:
            fig, ax = plt.subplots(figsize=(4, 4))  # ,dpi=300)

        mean = self.average()
        std = self.std()
        logger.debug("Persisting the dask arrays")
        mean["avg"] = mean["avg"].persist()
        std["std"] = std["std"].persist()

        bins = mean["bins"]
        num = mean["num"]
        bincenters = 0.5 * (bins[:-1] + bins[1:])

        # progress(mean['avg'], std['std'])
        logger.debug("Getting final result")
        mean = mean["avg"].compute()
        std = std["std"].compute()
        logger.debug("Plotting")

        mul = 100
        ax.plot(bincenters * mul, bincenters * mul * 0, color="blue")
        ax.plot(bincenters * mul, bincenters * mul, color="blue", ls="--")

        ax.plot(
            bincenters * mul,
            mean * mul,
            label="Planck 18$\\times$BORG (SDSS3-BOSS)",
            color="black",
        )
        ax.plot(bincenters * mul, mean * mul, marker="o", color="black")
        ax.fill_between(
            bincenters * mul,
            (mean - std) * mul,
            (mean + std) * mul,
            color="gray",
            alpha=0.5,
        )

        ax.set_ylim(np.array([-0.01, 0.01]) * mul)
        ax.set_xlim(np.array([-0.01, 0.01]) * mul)

        ax.set_title(f"Number of samples: {num}")
        ax.set_aspect("equal")
        ax.xaxis.set_major_locator(plt.MultipleLocator(0.005 * mul))
        ax.yaxis.set_major_locator(plt.MultipleLocator(0.005 * mul))
        ax.ticklabel_format(style="sci")

        plt.legend()

        plt.xlabel(r"$\kappa_{\rm{BORG}} (\times 10^{-2})$")
        plt.ylabel((r"$\kappa_{\rm{Planck}} (\times 10^{-2})$"))


# fig.savefig(os.path.join(PLOT_PATH,"sdss_lensing.pdf"),bbox_inches='tight')

__all__ = ["CMBData", "PlanckLensingData", "BORGKappa", "LensingCrossCorrelation"]
