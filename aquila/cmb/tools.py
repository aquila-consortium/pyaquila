import numpy as np
import healpy as hp


def identify_cluster(field, thr=100):
    def rec(idx, incluster, nside):
        # get neighbours
        ingb = hp.get_all_neighbours(nside, np.array(incluster))

        cond = np.zeros(np.shape(idx), dtype=bool)
        for ii in ingb:
            cond += np.in1d(idx, ii)

        foo = np.where(cond)

        # these are the neighbors
        new_ngb = idx[foo]

        while len(new_ngb > 0):

            # eliminate neighbors from list
            idx = np.delete(idx, foo)

            for n in new_ngb:
                incluster.append(n)

            # get neighbours
            ingb = hp.get_all_neighbours(nside, np.array(incluster))

            cond = np.zeros(np.shape(idx), dtype=bool)
            for ii in ingb:
                cond += np.in1d(idx, ii)

            foo = np.where(cond)

            # these are the neighbors
            new_ngb = idx[foo]

            # print(len(new_ngb),len(idx),len(incluster))

        return idx

    nside = hp.npix2nside(len(field))
    fr = np.zeros(np.shape(field))
    foo = np.where(field > thr)
    fr[foo] = 1
    nred = 4

    fr = hp.ud_grade(fr, nside // nred)

    idx = np.arange(len(fr))
    foo = np.where(fr > 0)
    idx = idx[foo]

    clusters = []

    cnt = 1
    while len(idx) > 0:
        # print('Processing cluster Nr.:',cnt)
        incluster = []
        i0 = idx[0]
        idx = np.delete(idx, 0)
        incluster.append(i0)
        idx = rec(idx, incluster, nside // nred)
        clusters.append(np.array(incluster))
        cnt += 1

    aux = np.zeros(len(fr))
    cnt = 1
    for inc in clusters:
        aux[inc] = cnt
        cnt += 1

    # aux[clusters[10]]=1
    aux = hp.ud_grade(aux, nside)
    # hp.mollview(aux,coord='gc')

    return aux
