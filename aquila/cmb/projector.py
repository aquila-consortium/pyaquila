import numpy as np
import healpy as hp


def get_spherical_slice(
    vdata, nside=32, xmin=np.array([0, 0, 0]), Larr=np.array([10, 10, 10]), rslice=150.0
):
    def RenderSphere(VolumeData3D, image, rslice, observer, Larr, Narr):
        print("Rendering Sphere...")

        rot = hp.Rotator(coord=["G", "C"])
        NSIDE = hp.npix2nside(len(image))

        idx = Larr[0] / Narr[0]
        idy = Larr[1] / Narr[1]
        idz = Larr[2] / Narr[2]

        ipix = np.arange(len(image))
        dx, dy, dz = hp.pix2vec(NSIDE, ipix)
        d = np.sqrt(dx * dx + dy * dy + dz * dz)
        dx = dx / d
        dy = dy / d
        dz = dz / d  # ray unit vector

        dx, dy, dz = rot(dx, dy, dz)

        rayX = observer[0] + rslice * dx
        rayY = observer[1] + rslice * dy
        rayZ = observer[2] + rslice * dz

        rayX /= idx
        rayY /= idy
        rayZ /= idz

        ix = (np.rint(rayX)).astype(int)
        iy = (np.rint(rayY)).astype(int)
        iz = (np.rint(rayZ)).astype(int)

        image *= np.nan

        jx = (ix + 1) % Narr[0]
        jy = (iy + 1) % Narr[1]
        jz = (iz + 1) % Narr[2]
        rx = rayX - ix
        ry = rayY - iy
        rz = rayZ - iz
        qx = 1.0 - rx
        qy = 1.0 - ry
        qz = 1.0 - rz

        foo = np.where(
            (ix > -1)
            * (ix < Narr[0])
            * (iy > -1)
            * (iy < Narr[1])
            * (iz > -1)
            * (iz < Narr[2])
        )

        ix = ix[foo]
        iy = iy[foo]
        iz = iz[foo]
        jx = jx[foo]
        jy = jy[foo]
        jz = jz[foo]
        rx = rx[foo]
        ry = ry[foo]
        rz = rz[foo]
        qx = qx[foo]
        qy = qy[foo]
        qz = qz[foo]

        image[foo] = VolumeData3D[
            ix, iy, iz
        ]  # * qx * qy * qz +VolumeData3D[ix,iy,jz] * qx * qy * rz +VolumeData3D[ix,jy,iz] * qx * ry * qz +VolumeData3D[ix,jy,jz] * qx * ry * rz +VolumeData3D[jx,iy,iz] * rx * qy * qz +VolumeData3D[jx,iy,jz] * rx * qy * rz +VolumeData3D[jx,jy,iz] * rx * ry * qz +VolumeData3D[jx,jy,jz] * rx * ry * rz;
        print("Done...")

    observer = np.array([0, 0, 0])
    obs = np.array(
        [observer[0] - xmin[0], observer[1] - xmin[1], observer[2] - xmin[2]]
    )

    Narr = np.shape(vdata)
    image = np.zeros(hp.nside2npix(nside))
    RenderSphere(vdata, image, rslice, obs, Larr, Narr)
    return image


def integrate_los(
    vdata,
    nside=32,
    xmin=np.array([0, 0, 0]),
    Larr=np.array([10, 10, 10]),
    rslice=np.array([150.0]),
):

    image = np.zeros(hp.nside2npix(nside))

    for r in rslice:
        image += get_spherical_slice(vdata, nside=nside, xmin=xmin, Larr=Larr, rslice=r)

    return image
