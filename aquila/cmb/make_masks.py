import numpy as np
from astropy.cosmology import Planck15 as cosmo
from astropy.io import fits
from astropy import units as u
import healpy as hp


def rDelta(m, zz, Delta):
    """Returns r_Delta"""
    hn = np.array([cosmo.H(z) / cosmo.H(0.0) for z in zz])
    HH = (cosmo.H(0.0) / 100.0).value
    RHOCRIT0 = (cosmo.critical_density(0).to(u.solMass / u.Mpc**3) / HH**2).value
    rhoc = RHOCRIT0 * hn * hn

    return (3 * m / (4 * np.pi * Delta * rhoc)) ** 0.333333333 * (1 + zz)


def sz_ps_mask(zmin=0, zmax=0.43, nside=2048):
    print(" Building SZ point source mask")

    HH = (cosmo.H(0.0) / 100.0).value

    # Read catalog and remove all clusters above z=0.43
    data = (fits.open("data/data_y/HFI_PCCS_SZ-union_R2.08.fits"))[1].data
    ids = np.where(data["REDSHIFT"] >= zmin)[0]
    data = data[ids]
    idsz = np.where(data["REDSHIFT"] < zmax)[0]
    data = data[idsz]
    # Compute their angular extent
    r500 = rDelta(data["MSZ"] * HH * 1e14, data["REDSHIFT"], 500)

    chi = (cosmo.comoving_distance(data["REDSHIFT"]).to(u.Mpc)).value
    th500 = r500 / chi

    # Compute angular positions for each cluster
    theta = (90 - data["GLAT"]) * np.pi / 180
    phi = data["GLON"] * np.pi / 180
    vx = np.sin(theta) * np.cos(phi)
    vy = np.sin(theta) * np.sin(phi)
    vz = np.cos(theta)
    # Generate mask by cutting out a circle of radius
    # 3*theta_500 around each cluster
    mask_sz = np.ones(hp.nside2npix(nside))
    for i in np.arange(len(data)):
        v = np.array([vx[i], vy[i], vz[i]])
        radius = 3 * th500[i]
        ip = hp.query_disc(nside, v, radius)
        mask_sz[ip] = 0
    return mask_sz
