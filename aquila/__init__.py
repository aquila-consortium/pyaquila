"""
aquila module
=============

`aquila` (or PyAquila) holds a number of statistical utilities, tensorflow
kernels, BORG chain analysis that are used by the Aquila consortium. This
package is an effort to consolidate the different approaches and algorithms so
that they can be reused at maximum efficiency.
"""

from . import clustering, icgen, utils, cmb, analysis, validation

from .logsystem import logger, log_context
