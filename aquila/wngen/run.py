def convert_sample(mcmc_path, sample_id, output):
    import h5py as h5
    import numpy as np
    import struct

    path_mcmc = f"{mcmc_path}/mcmc_{sample_id}.h5"
    path_wn = f"{output}/wn_{sample_id}.dat"
    print(f"Converting MCMC {sample_id} ({path_mcmc}) to white noise ({path_wn}) ...")

    with h5.File(path_mcmc, mode="r") as f:
        s_hat = f["scalars/s_hat_field"][:]

    x = np.fft.irfftn(s_hat)
    x *= np.sqrt(x.size)

    with open(path_wn, mode="wb") as f:
        Nx, Ny, Nz = x.shape

        checkPoint = 4 * 4
        f.write(struct.pack("IIIIII", checkPoint, Nx, Ny, Nz, 0, checkPoint))

        x = x.reshape(x.shape, order="F")
        checkPoint = struct.pack("I", 4 * Ny * Nz)
        for i in range(Nx):
            f.write(checkPoint)
            f.write(x[i].astype(np.float32).tobytes())
            f.write(checkPoint)


def run():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--mcmc", help="Path to the MCMC chain (including restarts)", type=str
    )
    parser.add_argument(
        "samples",
        metavar="id",
        type=int,
        nargs="+",
        help="list of samples to convert from",
    )

    parser.add_argument(
        "--output-dir",
        type=str,
        default="./",
        help="Output directory of the white noise files",
    )

    args = parser.parse_args()

    for sample_id in args.samples:
        convert_sample(args.mcmc, sample_id, args.output_dir)
