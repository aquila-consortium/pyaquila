#!/usr/bin/env python

#from distutils.core import setup
from setuptools import setup, find_packages

setup(name='PyAquila',
      version='0.1.4',
      description='Python Distribution Utilities',
      install_requires=['numpy'],
      requires=['numpy'],
      author='Guilhem Lavaux',
      author_email='guilhem.lavaux@iap.fr',
      long_description=open("./README.rst", 'r').read(),
      long_description_content_type="text/markdown",
      url='https://www.aquila-consortium.org/',
      keywords="cosmology, inference, simulation, statistics",
      packages=find_packages(include=['aquila', 'aquila.*']),
      classifiers=["Intended Audience :: Developers",
                   "Natural Language :: English",
                   "Programming Language :: Python",
                   "Programming Language :: Python :: 3"],
      python_requires='>=3.6',
      scripts=["scripts/aquila_gen_ic"],
      entry_points={"console_scripts":[
           "aquila_gen_wn=aquila.wngen.run:run","aquila_gen_kappa=aquila.kappa:run",
           "aquila_gen_ic=aquila.icgen:run"
      ]}
)
