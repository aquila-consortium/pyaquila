import numpy as np
import aquila.tf.kernels as ak
import tensorflow as tf

g = tf.random.get_global_generator()
x = g.uniform((1, 8, 8, 8, 1), dtype=tf.float32)
#z = g.uniform((1000, ), minval=0, maxval=23, dtype=tf.int32)


class SimpleU(tf.Module):
  def __init__(self):
    super(SimpleU, self).__init__()
    self.u = ak.UNet(input_filters=1, n_steps=3, conv_kernel=2, n_filters=1, n_out=1)

  def __call__(self, x):
    return self.u(x_in=x, leaky_param=0.01)

uu = SimpleU()
uu(x)
print(type(uu.variables))
print(list(a.name for a in uu.variables))
