import tensorflow as tf
from aquila.tf.kernels import spatial_invariance

g = tf.random.get_global_generator()
x = g.uniform((1, 8, 8, 8, 1), dtype=tf.float32)
z = g.uniform((1000, ), minval=0, maxval=23, dtype=tf.int32)

r = tf.map_fn(lambda a: spatial_invariance(a, x)[0, ...],
              z,
              fn_output_signature=tf.float32)
print(r)
