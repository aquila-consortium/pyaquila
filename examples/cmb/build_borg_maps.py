#!/usr/bin/env python3
import numpy as np
import healpy as hp
from aquila.cmb.projector import integrate_los


#setup grid
rmax  = 150. # maximum radius around the observer [Mpc/h]
rmin  = 0.   # minimum radius around the observer [Mpc/h]	
dx    = 0.5 # los step [Mpc/h]		
steps = np.rint(rmax/dx)
rr=np.linspace(rmin,rmax,steps)

#setup healpix
NSIDE=1024

#load BORG data
dat=np.load('data/data_borg/mean_density_chain_hires.npz')

#integrate mean density
print('Reading mu-field')
array=dat['mu_f']

#build power-series maps
npower=10
for n in np.arange(npower):
	print('Build map for power index n=',n)
	aux=integrate_los(array**n,nside=NSIDE,xmin=np.array([-677.7*0.5,-677.7*0.5,-677.7*0.5]),Larr=np.array([677.7,677.7,677.7]) ,rslice = rr)
	hp.write_map("data/data_borg/map_mu_rho_power_"+str(n)+".fits",aux,overwrite=True)


aux=integrate_los(array,nside=NSIDE,xmin=np.array([-677.7*0.5,-677.7*0.5,-677.7*0.5]),Larr=np.array([677.7,677.7,677.7]) ,rslice = rr)
print('Writing:',"data/data_borg/map_mu_rho.fits")
hp.write_map("data/data_borg/map_mu_rho.fits",aux,overwrite=True)


#integrate variance of density
print('Reading var-field')
array=dat['var_f']
aux=integrate_los(array,nside=NSIDE,xmin=np.array([-677.7*0.5,-677.7*0.5,-677.7*0.5]),Larr=np.array([677.7,677.7,677.7]) ,rslice = rr)
print('Writing:',"data/data_borg/map_var_rho.fits")
hp.write_map("data/data_borg/map_var_rho.fits",aux,overwrite=True)


	





