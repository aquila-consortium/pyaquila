#!/usr/bin/env python3
from aquila.cmb.make_masks import sz_ps_mask 
import healpy as hp

#we want to study structures in the nearby Universe
#therefore we exclude all known clusters at z > 0.05
#mask_sz=sz_ps_mask(zmin=0.,zmax=0.43)
mask_sz=sz_ps_mask(zmin=0.05,zmax=1000)
hp.write_map("data/data_y/mask_sz.fits",mask_sz,overwrite=True)

#select known clusters in the low-z range
mask_known=sz_ps_mask(zmin=0.0,zmax=0.05)
hp.write_map("data/data_y/mask_known_sz.fits",mask_known,overwrite=True)


#Make mask
print ("Generate Masks for the Galaxy ...")
mask_gal_80=hp.read_map("data/data_y/HFI_Mask_GalPlane-apo0_2048_R2.00.fits",verbose=False,field=4)
mask_gal_60=hp.read_map("data/data_y/HFI_Mask_GalPlane-apo0_2048_R2.00.fits",verbose=False,field=2)
mask_gal_40=hp.read_map("data/data_y/HFI_Mask_GalPlane-apo0_2048_R2.00.fits",verbose=False,field=1)
mask_gal_20=hp.read_map("data/data_y/HFI_Mask_GalPlane-apo0_2048_R2.00.fits",verbose=False,field=0)

mask_p0=hp.read_map("data/data_y/LFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,hdu=1); print (np.mean(mask_p0))
mask_p1=hp.read_map("data/data_y/LFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,hdu=2); print (np.mean(mask_p1))
mask_p2=hp.read_map("data/data_y/LFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,hdu=3); print (np.mean(mask_p2))

mask_pl=mask_p0*mask_p1*mask_p2

mask_p0=hp.read_map("data/data_y/HFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,field=0); print (np.mean(mask_p0))
mask_p1=hp.read_map("data/data_y/HFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,field=1); print (np.mean(mask_p1))
mask_p2=hp.read_map("data/data_y/HFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,field=2); print (np.mean(mask_p2))
mask_p3=hp.read_map("data/data_y/HFI_Mask_PointSrc_2048_R2.00.fits",verbose=False,field=3); print (np.mean(mask_p3))

mask_ph=mask_p0*mask_p1*mask_p2*mask_p3

hp.write_map("data/data_y/mask_planck20.fits",mask_gal_20*mask_ph,overwrite=True)
hp.write_map("data/data_y/mask_planck40.fits",mask_gal_40*mask_ph,overwrite=True)
hp.write_map("data/data_y/mask_planck60.fits",mask_gal_60*mask_ph,overwrite=True)
hp.write_map("data/data_y/mask_planck80.fits",mask_gal_80*mask_ph,overwrite=True)

hp.write_map("data/data_y/mask_planck60L.fits",mask_gal_60*mask_ph*mask_pl,overwrite=True)
hp.write_map("data/data_y/mask_planck80L.fits",mask_gal_80*mask_ph*mask_pl,overwrite=True)
hp.write_map("data/data_y/mask_planck20S.fits",mask_gal_20*mask_ph*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck40S.fits",mask_gal_40*mask_ph*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck60S.fits",mask_gal_60*mask_ph*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck80S.fits",mask_gal_80*mask_ph*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck20LS.fits",mask_gal_20*mask_ph*mask_pl*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck40LS.fits",mask_gal_40*mask_ph*mask_pl*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck60LS.fits",mask_gal_60*mask_ph*mask_pl*mask_sz,overwrite=True)
hp.write_map("data/data_y/mask_planck80LS.fits",mask_gal_80*mask_ph*mask_pl*mask_sz,overwrite=True)

