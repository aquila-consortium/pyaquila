#!/usr/bin/env python3
from aquila.cmb import identify_clusters
import healpy as hp


#read data
rho_mu    = hp.read_map('data/data_borg/map_mu_rho.fits')
rho_mu    = hp.ud_grade(rho_mu, 2048)

nsteps=5
thr_arr=np.linspace(60,100,nsteps+1)

for thr in thr_arr:
	print('Build clusters at threshold:', thr)
	aux=identify_cluster(rho_mu,thr=thr)
	print('Writing:',"data/data_borg/cluster_mask_"+str(thr) +".fits")
	hp.write_map("data/data_borg/cluster_mask_"+str(thr) +".fits",aux,overwrite=True)

