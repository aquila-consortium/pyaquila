import borg
import tensorflow as tf
import tensorflow_probability as tfp
import cosmotool as ct
import numpy as np
import aquila.tf as atf
tfd = tfp.distributions
tfb = tfp.bijectors

from density_model import DensityModel
from chain import compute_chain
from tqdm import tqdm

cosmo = dict(omega_M_0=0.30, omega_B_0=0., h=0.7, ns=0.97, sigma8=0.8)

density = DensityModel(cosmo)
data, signal, ic_ref = density.mock_data()

a_gibbs = compute_chain(density,
                        data,
                        gibbs=True,
                        Nsamples=1000,
                        step_size=0.002)

a_avg = np.average(a[2:], axis=0)
a_avg_gibbs = np.average(a_gibbs, axis=0)
print(a_avg[0], np.std(a[2:, 0]))
print(a_avg_gibbs[0], np.std(a_gibbs[2:, 0]))
