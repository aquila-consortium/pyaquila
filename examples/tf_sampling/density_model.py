import borg
import tensorflow as tf
import tensorflow_probability as tfp
import cosmotool as ct
import numpy as np
import aquila.tf as atf
tfd = tfp.distributions
tfb = tfp.bijectors


@tf.function
def powerEfstathiou(k, Gamma0=1.0, norm=1.0, ns=0.97, h=0.7):
    k = k * h
    a = 6.4 / Gamma0
    b = 3 / Gamma0
    c = 1.7 / Gamma0
    nu = 1.13
    f = (a * k) + (b * k)**(1.5) + (c * k)**2

    return norm * k**ns * ((1 + f**nu)**(-2 / nu)) / h**3


class DensityModel:
    def __init__(self, cosmo):
        self.nmean = tf.constant(10., dtype=tf.float32)
        self.bias = tf.constant(1.0, dtype=tf.float32)
        self.bias_q = tf.constant(0.1, dtype=tf.float32)

        self.L = 500.
        self.N = 8

        self.unitGauss = tfd.Normal(0.0, 1.0)
        self.dataGauss = tfd.Normal(0.0, 0.1)
        self.biasGauss = tfd.Normal(0.0, 10.0)

        t_freq = tf.convert_to_tensor(
            np.fft.fftfreq(self.N, d=self.L / self.N).astype(np.float32))

        ik = t_freq * 2 * np.pi

        kn = tf.sqrt(ik[:,None,None]**2 + \
                     ik[None,:,None]**2+ik[None,None,:]**2 \
                    )

        P = ct.CosmologyPower(**cosmo)
        P.setFunction('EFSTATHIOU')
        P.normalize(cosmo['sigma8'])
        P.compute(0.1)
        self.P = P
        self.Gamma0 = cosmo['omega_M_0'] * cosmo['h']**2

        # Normalize the power spectrum
        self.NormPower = float(
            P.compute(0.1) / powerEfstathiou(
                0.1, Gamma0=self.Gamma0, h=cosmo['h'], ns=cosmo['ns']))

        # Compute the transfer function in k space (with Tensor object)
        self.Tk = tf.cast(
            tf.sqrt(
                powerEfstathiou(kn,
                                norm=self.NormPower,
                                Gamma0=self.Gamma0,
                                h=cosmo['h'],
                                ns=cosmo['ns']) * self.L**3), tf.complex64)
        self.cosmo = cosmo
        self.Pref = P
        self.sharpness = tf.constant(10.0, dtype=tf.float32)

    def plot_Pk(self):
        import matplotlib.pyplot as plt
        k = 10**np.linspace(-3, 1, 1000)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.loglog(k,
                  np.array(
                      powerEfstathiou(k,
                                      norm=self.NormPower,
                                      Gamma0=self.Gamma0,
                                      h=self.cosmo['h'],
                                      ns=self.cosmo['ns'])),
                  label="TF")
        ax.loglog(k, self.P.compute(k), label="ref")
        ax.legend()
        return fig

    @tf.function
    def simulate(self, x):
        # Construct the complex field from a 2xNxNxN real field
        ghat = tf.complex(x[:, 0, ...] / tf.sqrt(2.0),
                          x[:, 1, ...] / tf.sqrt(2.0))
        #ghat = tf.signal.fft3d(tf.cast(x/N**(1.5),tf.complex64))

        ghat = ghat * self.Tk

        # Inverse transform + real part
        gcolored = tf.math.real(tf.signal.ifft3d(ghat)) * (self.N / self.L)**3
        # Say that the generated density (for fun) has a softplus to avoid having 1+delta going negative
        return tf.math.softplus(gcolored + 1) - 1

    def mock_data(self):
        x_mock = self.unitGauss.sample((2, self.N, self.N, self.N))
        ic_ref = tf.complex(x_mock[0, ...], x_mock[1, ...])
        signal = self.simulate(x_mock[tf.newaxis, ...])[0, ...]
        # Generation of mock data is a simulation followed by a sample from the noise
        # Here it is a Gaussian noise with variance nmean.
        # The biased field is 1 + b * delta + b2 * delta**2
        data = np.sqrt(self.nmean)*self.dataGauss.sample((self.N,self.N,self.N)) \
            + self.nmean*(1 + self.bias * signal + self.bias_q * signal**2)
        # Return data/signal/ic
        return data, signal, ic_ref

    def _unnormalized_log_prob_split(self, data, bias_linear, bias_quadratic, x):
        # Reshape the ic into 1x2xNxNxN (1 is just batch size)
        y = tf.reshape(x, ((1, 2, self.N, self.N, self.N)))
        # Apply a transform to the bias as we want to avoid the HMC going nut with sharp prior.
        bias = tf.math.softplus(bias_linear * self.sharpness) / self.sharpness
        # Build the density
        rho = self.simulate(y)[0, ...]
        # Apply bias
        z = bias * rho + bias_quadratic * rho**2
        # Apply final touch
        gcolored = self.nmean * (tf.constant(1, dtype=tf.float32) + z)
        # Return log_probability of all the components
        #       bias_linear <- gaussian prior variance 1
        #       bias_quadratic <- same
        #       y <- gaussian prior variance 1 (N-d)
        #       data <- gauss variance nmean
        return self.biasGauss.log_prob(bias_linear) + self.biasGauss.log_prob(bias_quadratic) \
                + tf.reduce_sum( self.unitGauss.log_prob(y)) + \
                tf.reduce_sum(self.dataGauss.log_prob((gcolored - data)/tf.sqrt(self.nmean)))

    def unnormalized_log_prob(self, data, x):
        # Main entry point of the posterior: data and proposed sample for the parameter space
        # the proposed point is a real space vector of adequate length to store everything
        # we cut it into the bias (1 and 2) and the rest for ic
        return self._unnormalized_log_prob_split(data, x[0], x[1], x[2:])
