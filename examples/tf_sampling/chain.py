import borg
import tensorflow as tf
import tensorflow_probability as tfp
import cosmotool as ct
import numpy as np
import aquila.tf.algorithms as atf
from functools import partial
tfd = tfp.distributions
tfb = tfp.bijectors

from density_model import DensityModel
from tqdm import tqdm


# Run the chain (with burn-in).
@tf.function
def run_tf_chain(s, kernel, tracer):
    # Run the chain (with burn-in).
    s = [s]
    for j in range(10):
        # This sample the posterior using kernel "kernel"
        # The sampler will run 1000 loops before exiting
        # Start point is current_state (i.e. last element of s)
        s, is_accepted = tfp.mcmc.sample_chain(num_results=1,
                                               current_state=s[-1],
                                               num_steps_between_results=1000,
                                               num_burnin_steps=0,
                                               kernel=kernel,
                                               trace_fn=tracer)

    # Return the state
    return s, is_accepted


def compute_chain(density,
                  data,
                  gibbs=False,
                  starter=None,
                  use_nuts=False,
                  step_size=0.05,
                  Nsamples=500):

    # Build an HMC/NUTS kernel for the target log probability target_prob_fn
    def make_hmc(target_prob_fn):
        if use_nuts:
            return tfp.mcmc.NoUTurnSampler(target_log_prob_fn=target_prob_fn,
                                           step_size=step_size)
        else:
            return tfp.mcmc.HamiltonianMonteCarlo(
                target_log_prob_fn=target_prob_fn,
                step_size=step_size,
                num_leapfrog_steps=100)

    # Build a slice sampler for the bias (target_prob_fn provides log probability)
    def make_bias_kernel(target_prob_fn):
        return tfp.mcmc.SliceSampler(target_prob_fn, 0.01, max_doublings=1)

    # Build an HMC sampler for the bias
    def make_bias_hmc(target_prob_fn):
        return tfp.mcmc.HamiltonianMonteCarlo(target_prob_fn,
                                              step_size=0.001,
                                              num_leapfrog_steps=100)

    N = density.N

    # If we do gibbs, we split the sampling of the space into sub-elements.
    if gibbs:
        kernel = atf.gibbs.Gibbs(
            partial(density.unnormalized_log_prob, data),
            [
                make_bias_hmc,  # Can be replaced with make_bias_kernel, sample bias 1
                make_bias_hmc,  # Can be replaced with make_bias_kernel, sample bias 2
                make_hmc        # Sample density
            ],
            [1, 1, 2 * N**3])   # Size of the space for bias 1=1, for bias 2=1, for density= 2*N**3
        # Now build a function to collect the acceptance of all those kernels
        # It may not work for slice_sampler
        tracer = lambda _, prev_results: [
            o.log_accept_ratio for o in prev_results.other_results
        ]
    else:
        # Build a single giant HMC/NUTS
        hmc = make_hmc(density.unnormalized_log_prob)

        # Bias parameters need a prescaling, they do not behave like density.
        scaler = np.ones(2 * N**3 + 2, dtype=np.float32)
        scaler[0] = 0.01
        scaler[1] = 0.001
        # Transform the kernel, the jacobian will be applied
        kernel = tfp.mcmc.TransformedTransitionKernel(
            inner_kernel=hmc, bijector=tfb.ScaleMatvecDiag(scale_diag=scaler))

        # Build the tracer
        tracer = lambda _, prev_results: prev_results.inner_results.log_accept_ratio

    # Build the initial state
    init_state = np.zeros((2 * N**3 + 2), dtype=np.float32)
    init_state[0] = 1.0
    init_state[1] = 0.0
    if starter is not None:
        init_state[2:] = starter

    # Convert this into a TF tensor
    state = tf.convert_to_tensor(init_state)

    # Allocate memory for storage of the chain
    all_samples = np.empty((Nsamples, 2 * N**3 + 2), dtype=np.float32)

    with tqdm(range(Nsamples)) as a:
        for _i in a:
            # Run the chain a bit
            samples, is_accepted = run_tf_chain(state, kernel, tracer)
            # Report acceptance ratio
            a.set_postfix(accept=" ".join(repr(a.numpy()[0]) for a in is_accepted))
            burnin = False
            state = samples[-1]
            # Store
            all_samples[_i] = samples

    return all_samples
