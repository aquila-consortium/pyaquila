import aquila.tf.kernels as kernels
import tensorflow as tf


Lbox = 1.0

cic = kernels.CICProjection(Lbox, 32)

na = tf.newaxis

position = tf.constant([[0.5,0.5,0.5]])[na,...]
print(position.shape)

rho = cic.project(position)