import aquila.tf.kernels as kernels
import tensorflow as tf

r=tf.random.normal((16,16,16,3))

r2 = kernels.compute_divergence_field(r)
r3 = kernels.compute_divergence_field(r,periodic=True)
print(r2.shape,r3.shape)


s=tf.random.normal((16,16,16))
t = kernels.compute_gradient_field(s)

t2 = kernels.compute_gradient_field(s, periodic=True)

