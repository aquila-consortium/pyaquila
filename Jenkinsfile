pipeline {
    agent { node { label 'ubuntu'; } }

    options {
       buildDiscarder(logRotator(numToKeepStr: '5'))
    }

    triggers {
      pollSCM('H H(0-7) * * *')
    }


    environment {
       DOC_DEPLOYER = credentials('Doc deployment')
    }

    stages {
        stage('Preparation') {
            steps {
                script {
                    branchName = 'develop'
                    cred = 'aquila_access' 

                    env.PYTHON_VENV = """${sh(
                       returnStdout:true,
                       script: 'BTAG=$(echo ${BUILD_TAG} | sed "s,%,,g"); echo -n "${WORKSPACE}/${BTAG}"'
                     )}"""
                }
            }
        }
        stage('Source') { // for display purposes
            steps {
                git branch: branchName, credentialsId: cred, url: 'https://git.aquila-consortium.org/aquila-consortium/pyaquila/'

                sh 'python3 -m venv ${PYTHON_VENV}'
                sh 'ls && echo ${PYTHON_VENV} && ls ${PYTHON_VENV}'
                sh 'test -e ${PYTHON_VENV}'
            }
        }
        stage('Build') {
            steps {
                ansiColor('xterm') {
                    sh '''
                      . ${PYTHON_VENV}/bin/activate
                      CMAKE_PREFIX_PATH=${VIRTUAL_ENV}:/opt/boost
                      export CMAKE_PREFIX_PATH
                      python3 setup.py build
                      python3 setup.py sdist
                    '''
                }
            }
        }
        stage('Tests') {
            steps {
                dir('build') {
                    sh """
                      . ${PYTHON_VENV}/bin/activate
                      pip3 install wheel pytest jax[cpu] numpy deprecated
                    """
                }
            }
        }
        stage("Doc") {
            steps {
                dir('docs') {
                    sh """
                      . ${PYTHON_VENV}/bin/activate
                      export PYTHONWARNINGS=always::ImportWarning
                      pip3 install wheel
                      pip3 install -r requirements.txt
                      rm -fr source/_generate
                      rm -fr _build
                      make html
                      tar -C _build/html -zcvf doc.tgz .
                      curl -v -F filename=doc -F file=@doc.tgz http://athos.iap.fr:9595/deploy-doc2/$DOC_DEPLOYER/pyaquila-develop
                    """
                }
            }
        }
    }
    post {
        failure {
            notifyBuild("FAIL")
        }
        success {
            notifyBuild("SUCCESS")
        }
        always {

            /* clean up our workspace */
            deleteDir()
            /* clean up tmp directory */

            dir("${PYTHON_VENV}") {
                deleteDir()
            }
            dir("${workspace}@tmp") {
                deleteDir()
            }
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus = buildStatus ?: 'SUCCESS'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#0000FF'
  } else if (buildStatus == 'SUCCESS') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>
    <p>Build status: <span style='color: ${colorCode};'>${buildStatus}</span></p>"""

  emailext (
      mimeType: 'text/html',
      subject: subject,
      body: details,
      recipientProviders: [developers(), requestor()]
    )
}
